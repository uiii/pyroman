#include "global.h"

#include "timer.h"
#include "game.h"

int main(int argc, char* argv[])
{
	initSDL();

	PM::Game game;
	PM::Timer fps;

	// main loop
	while(PM::Global::quit == false)
	{
		game.update();
		game.render();
	}
	
	return 0;
}
