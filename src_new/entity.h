#ifndef PM_ENTITY_H
#define PM_ENTITY_H

#include "global.h"

namespace PM
{

class Entity
{
public:
	Entitiy(PM::Position _position):
		position(_position)
	{}
	
	~Entity() = 0;

	const PM::Positon* getPosition() { return this->position; }

protected:
	PM::Position position;
};

class EntityController
{
public:
	EntityController(PM::Entity* _entity):
		entity(_entity)
	{}

	~EntityController() = 0;

private:
	Entity* entity;
};

class EntityView
{
public:
	EntityView(PM::Entity* _entity):
		entity(_entity)
	{}
	
	~EntityView() = 0;

private:
	const Entity* entity;
};

}

#endif
