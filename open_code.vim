winpos 10 10

set lines=71
set columns=234


tabnew src/game.cpp
vsplit src/game.h
tabnew src/map.cpp
vsplit src/map.h
tabnew src/tile.cpp
vsplit src/tile.h
tabnew src/character.cpp
vsplit src/character.h
tabnew src/pathfinding/nodemap.cpp
vsplit src/pathfinding/nodemap.h
tabnew src/pathfinding/pathfinding.cpp
vsplit src/pathfinding/pathfinding.h
tabnew src/pathfinding/nodeheap.cpp
vsplit src/pathfinding/nodeheap.h
tabnew src/bomb.cpp
vsplit src/bomb.h
tabnew src/timer.cpp
vsplit src/timer.h
tabnew src/global.cpp
vsplit src/global.h
tabnew src/exception.h
