#ifndef PM_TIMER_H
#define PM_TIMER_H

#include <string>
#include <map>

#include "global.h"

namespace PM
{

class Timer
{
public:
	Timer();
	
	virtual void start(const std::string& _name = "_noname_");
	virtual void stop(const std::string& _name = "_noname_");
	
	virtual void pause(const std::string& _name = "_noname_");
	virtual void unpause(const std::string& _name = "_noname_");

	virtual int getTicks(const std::string& _name = "_noname_");

	virtual bool isStarted(const std::string& _name = "_noname_");
	virtual bool isPaused(const std::string& _name = "_noname_");

	virtual bool find(const std::string& _name);

protected:
	std::map<std::string, int> startTicks;
	std::map<std::string, int> pausedTicks;

	std::map<std::string, bool> paused;
	std::map<std::string, bool> started;
};

class FPSTimer : public Timer
{
public:
	virtual void start(const std::string& _name = "_noname_");
	
	virtual void pause(const std::string& _name = "_noname_");
	virtual void unpause(const std::string& _name = "_noname_");

	virtual int getTicks(const std::string& _name = "_noname_");
};

}

#endif
