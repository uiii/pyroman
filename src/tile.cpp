#include "tile.h"

#include <iostream>
#include <cstdlib>
#include <ctime>

#include "map.h"
#include "character.h"
#include "bomb.h"

namespace PM
{

Tile::Tile(PM::Map& _map, int _x, int _y, TileType _type):
	map(_map),
	position(_x, _y),
	type(_type),
	clear(true),
	currentState(0),
	improvement(NULL)
{
	this->setTexture();

	view.resize(4, NULL);
	viewDistance.resize(4, 0);
	
//	srand(time(NULL));
}

int Tile::setImprovement()
{
	if(this->type == WALL)
	{
		PM::ImprovementType improvementType;

		int random = rand() % 10;

		if(random <= 4)
		{
			improvementType = BOMB;
		}
		else if(random >= 5)
		{
			improvementType = RANGE;
		}

		random = rand() % 10;

		if(random > 2 && random < 7)
		{
			this->improvement = new PM::Improvement(improvementType, this->position);
			return 1;
		}
		else
		{
			this->improvement = NULL;
			return 0;
		}
	}
	
	return 0;
}

bool Tile::hasImprovement()
{
	if(this->improvement == NULL)
	{
		return false;
	}
	else
	{
		return true;
	}
}

void Tile::setTexture()
{
	switch(this->type)
	{
		case PASSAGE:
		{
			int y;

			this->texture.x = 0;

			if(this->clear == true)
			{
				this->texture.y = 0;
			}
			else
			{
				int coeficient = PM::Global::bombTimeToExplode / 3;

				if(this->currentState > coeficient * 2) this->texture.y = 20;
				else if(this->currentState > coeficient) this->texture.y = 40;
				else if(this->currentState > 0) this->texture.y = 60;
				else this->texture.y = 80;
			}

			this->texture.y = 0;

			break;
		}

		case WALL:
		{
			this->texture.x = 20;
			this->texture.y = (rand() % 3) * 20;

			break;
		}

		case UWALL:
		{
			this->texture.x = 40;
			this->texture.y = 0;

			break;
		}
	}

	this->texture.w = 20;
	this->texture.h = 20;
}

void Tile::setState()
{
	int RTTE = -PM::Global::bombTimeToVaporize; // remaining time to explode

	for(int i = 0; i < this->bombs.size(); i++)
	{	
		if(this->bombs[i]->getRemainingTime() < RTTE || RTTE == -PM::Global::bombTimeToVaporize)
		{
			RTTE = this->bombs[i]->getRemainingTime();
		}
	}

	if(bombs.size() == 0)
	{
		this->clear = true;
		this->currentState = 0;
	}
	else if(RTTE > 0)
	{
		this->clear = false;
		this->currentState = ((RTTE - 1) / 100) * 100;
	}
	else
	{
		this->clear = false;
		this->currentState = ((RTTE / 100) - 1) * 100;
	}
}

int Tile::getState(int _plusTime)
{
	std::cout << "tile " << this->currentState << " " << _plusTime << std::endl;
	if(this->currentState - _plusTime < -PM::Global::bombTimeToVaporize)
		return 0;
	else
		return this->currentState - _plusTime;
}

bool Tile::isClear(int _plusTime)
{
	std::cout << "tile " << this->currentState << " " << _plusTime << std::endl;
	if(this->currentState - _plusTime < -PM::Global::bombTimeToVaporize)
		return true;
	else
		return this->clear;
}

void Tile::placeCharacter(PM::Character* _character)
{
	this->characters.push_back(_character);
	_character->setTile(this);
}

void Tile::removeCharacter(PM::Character* _character)
{
	std::vector< PM::Character* >::iterator it;
	for(it = characters.begin(); it < characters.end(); it++)
	{
		if(*it == _character)
		{
			characters.erase(it);
			break;
		}
	}
}

void Tile::placeBomb(PM::Bomb* _bomb)
{
	this->bombs.push_back(_bomb);
	_bomb->addTile(this);

	this->setState();
}

void Tile::removeBomb(PM::Bomb* _bomb)
{
	std::vector<PM::Bomb*>::iterator it;
	for(it = bombs.begin(); it < bombs.end(); it++)
	{
		if(*it == _bomb)
		{
			bombs.erase(it);
			break;
		}
	}

	this->setState();
}

void Tile::control()
{
	this->setState();

	if(this->currentState < 0)
	{
		for(int i = 0; i < this->characters.size(); i++)
		{
			this->characters[i]->die();
		}

		for(int i = 0; i < this->bombs.size(); i++)
		{
			if(this->bombs[i]->getPosition() == this->position && this->bombs[i]->isExploding() == false)
			{
				bombs[i]->setTimeToExplode(500);
			}
		}
	}

	if(this->improvement != NULL)
	{
		// if there are one or more characters in the same time,
		// random will determine who gets the improvement
		if(this->characters.size() > 0)
		{
			int random = rand() % characters.size();
			this->characters[random]->improve(this->improvement);
			this->signal_improvementRemoved.emit(this->improvement);
			std::cout << "ted - tile" << std::endl;
			delete this->improvement;
			this->improvement = NULL;
		}
	}

	if(this->type == PASSAGE) this->setTexture();
}

void Tile::destroyRequest()
{
	if(this->type == WALL)
	{
		// send signal about wall destruction
		this->signal_wallDestroyed.emit();

		// if this tile is wall, change the views of neighbours
		PM::Tile *tile = NULL;
		int invers[4] = {1, 0, 3, 2};

		for(int i = 0; i < 4; i++)
		{
			PM::Position position = this->position;
			for(int j = 0; j < this->viewDistance[i]; j++)
			{
				position = position.move(i + 1);

				tile = this->map.getTile(position);
	
				tile->setView(invers[i], this->view[invers[i]]);
				tile->setViewDistance(invers[i], tile->getViewDistance(invers[i]) + this->viewDistance[invers[i]]);
			}
		}
	}

	// send signal about improvement
	if(this->improvement != NULL)
	{
		if(this->type == WALL) this->signal_improvementFound.emit(this->improvement);
		else if(this->type == PASSAGE)
		{
			this->signal_improvementRemoved.emit(this->improvement);

			delete this->improvement;
			this->improvement = NULL;
		}
	}

	this->type = PASSAGE;
	this->setTexture();

	this->setState();
}

void Tile::show(SDL_Surface* _surface)
{
	SDL_Rect target;
	target.x = this->position.x * 20;
	target.y = this->position.y * 20;

	PM::applySurface(&this->texture, PM::Global::textureSource, &target, _surface);

	if(this->improvement != NULL && this->type == PASSAGE)
	{
		this->improvement->show(_surface);
	}

	for(int i = 0; i < bombs.size(); i++)
	{
		// show bomb only when is placed here (dont show bomb's range)
		if(bombs[i]->getPosition() == this->position || bombs[i]->isExploding() == true)
		{
			bombs[i]->show(_surface, this->position);
		}
	}

	for(int i = 0; i < characters.size(); i++)
	{
		characters[i]->show(_surface);
	}
}

}
