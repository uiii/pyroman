#ifndef PM_PATHFINDING_H
#define PM_PATHFINDING_H

#include "../global.h"
#include "../tile.h"

#include "nodeheap.h"

namespace PM
{
	namespace PathFinding
	{

	class NodePath; // forward declaration
	class NodeMap; // forward declaration

	int tileCost(TileType type);

	enum FindOptions
	{
		TILE_TYPE = 1,
		TILE_IS_CLEAR = 2,
		PLAYER_DISTANCE = 4,
		TILE_POSITION = 8,
		TILE_NOT_POSITION = 16,

		TILE_TO_BOMB_PLAYER = 32,

		TILE_VIEW_POSITION = 64,

		TILE_VIEW_TYPE = 128,
		TILE_VIEW_IS_CLEAR = 256,

		IGNORE_WALLS = 512,
		IGNORE_UWALLS = 1024,

		IGNORE_POSITION = 2048,

		MAX_LENGTH = 4096,

		CONSIDER_BOMBS = 8192
	};
	
	struct FindProperties
	{
		FindProperties():
			type(WALL),
			playerXDistance(-1),
			playerYDistance(-1),
			position(0, 0),
			notPosition(0, 0),
			viewPosition(0, 0),
			viewDistance(0),
			viewType(WALL),
			ignorePosition(0, 0),
			maxLength(-1)
		{}

		int type;
		int playerXDistance;
		int playerYDistance;
		PM::Position position;
		PM::Position notPosition;

		PM::Position viewPosition;
		int viewDistance;
		int viewType;

		PM::Position ignorePosition;

		int maxLength;
	};

	// check if this tile matches specified options
	bool checkTile(NodeMap& map, PM::Tile* tile, int options, FindProperties properties, int plusTime = 0);

	// finds the shortest path to a tile with specified properties
	NodePath* findTile(NodeMap& map, PM::Position start, int options, FindProperties properties, int plusTime = 0);
 	
	}
}

#endif
