#include "pathfinding.h"

#include "../map.h"

#include "nodemap.h"
#include "nodeheap.h"

namespace PM
{
	namespace PathFinding
	{

	int tileCost(TileType _type)
	{
		int cost = 0;

		switch(_type)
		{
			case PASSAGE: cost = 1; break;
			case WALL: cost = 5; break;
			case UWALL: cost = 1000; break;
			default: cost = 1000; break;
		}

		return cost;
	}

	bool checkTile(NodeMap& _map, PM::Tile* _tile, int _options, const FindProperties _properties, int _plusTime)
	{
		bool wantedTileFound = true;
		int xDistance, yDistance;

		if(_options & TILE_TYPE)
		{
			//std::cout << _properties.type << " " << neighbourNode->getTile()->getType() << std::endl;
			if((_properties.type & _tile->getType()) == 0)
			{
				std::cout << "type" << std::endl;
				wantedTileFound = false;
				return false;
			}
		}

		if(_options & TILE_IS_CLEAR)
		{
			if(_tile->isClear(_plusTime) == false)
			{
				wantedTileFound = false;
				return false;
			}
		}

		if(_options & PLAYER_DISTANCE)
		{
			xDistance = abs(_tile->getPosition().x - _map.getTileMap().getPlayer()->getPosition().x);
			yDistance = abs(_tile->getPosition().y - _map.getTileMap().getPlayer()->getPosition().y);
			if(xDistance > _properties.playerXDistance || yDistance > _properties.playerYDistance)
			{
				std::cout << "player_distance" << std::endl;
				wantedTileFound = false;
				return false;
			}
		}

		if(_options & TILE_POSITION)
		{
			if(_tile->getPosition() != _properties.position)
			{
				std::cout << "position: ("
					<< _tile->getPosition().x << ", "
					<< _tile->getPosition().y << ") ("
					<< _properties.position.x << ", "
					<< _properties.position.y << ")" << std::endl;
				wantedTileFound = false;
				return false;
			}
		}

		if(_options & TILE_NOT_POSITION)
		{
			if(_tile->getPosition() == _properties.notPosition)
			{
				wantedTileFound = false;
				return false;
			}
		}

		if(_options & TILE_TO_BOMB_PLAYER)
		{
			bool wantedHorizontalFound = true;
			bool wantedVerticalFound = true;

			int horizontalDirection = -1;
			int verticalDirection = -1;

			PM::Position playerPosition = _map.getTileMap().getPlayer()->getPosition();
			PM::Position tilePosition = _tile->getPosition();

			xDistance = abs(tilePosition.x - playerPosition.x);
			yDistance = abs(tilePosition.y - playerPosition.y);
						
			std::cout << "tile_to_bomb_player" << _tile->getPosition() << std::endl;

			if(xDistance > 3 && yDistance > 3)
			{
				wantedTileFound = false;
				return false;
			}
			else
			{
				if(playerPosition.x <= tilePosition.x)		horizontalDirection = 2;
				else if(playerPosition.x > tilePosition.x)	horizontalDirection = 3;

				if(playerPosition.y <= tilePosition.y)		verticalDirection = 0;
				else if(playerPosition.y > tilePosition.y)	verticalDirection = 1;

				if(_tile->getViewDistance(horizontalDirection) <= xDistance)
				{
					// nevidi policko na souradnici hrace
					wantedHorizontalFound = false;
					std::cout << "false1" << std::endl;
				}
				else
				{
					if(_map.getTileMap().getTile(PM::Position(playerPosition.x, tilePosition.y))->getViewDistance(verticalDirection) <= yDistance)
					{
						// nevidi na hrace
						wantedHorizontalFound = false;
						std::cout << "false2" << std::endl;
					}
				}

				if(_tile->getViewDistance(verticalDirection) <= yDistance)
				{
					// nevidi policko na souradnici hrace
					wantedVerticalFound = false;
					std::cout << "false3" << std::endl;
				}
				else
				{
					if(_map.getTileMap().getTile(PM::Position(tilePosition.x, playerPosition.y))->getViewDistance(horizontalDirection) <= xDistance)
					{
						// nevidi na hrace
						wantedVerticalFound = false;
						std::cout << "false4" << std::endl;
					}
				}
			}
			
			std::cout << wantedHorizontalFound << std::endl;
			std::cout << wantedVerticalFound << std::endl;
			//std::cin.get();

			if(wantedHorizontalFound == false && wantedVerticalFound == false)
			{
				wantedTileFound = false;
				std::cout << "false5" << std::endl;
				//std::cin.get();
				return false;
			}
		}

		if((_options & TILE_VIEW_TYPE)) // && _tile->getType() == _properties.type)
		{
			bool wantedViewFound = false;
			if(_properties.viewType == PASSAGE)
			{
				if(_options & TILE_VIEW_IS_CLEAR)
				{
					wantedTileFound = false;
				}
				else
				{
					for(int i = 0; i < 4; i++)
					{
						if((_tile->getViewDistance(i) > 1) && _tile->getViewDistance(i) > _properties.viewDistance)
						{
							wantedViewFound = true;
							break;
						}
					}
				}
			}
			else
			{
				for(int i = 0; i < 4; i++)
				{
					if(_tile->getView(i) == NULL) break;

					if(_tile->getView(i)->getType() == _properties.viewType)
					{
						if(_tile->getViewDistance(i) <= _properties.viewDistance)
						{
							if(_options & TILE_VIEW_IS_CLEAR)
							{
							   	if(_tile->getView(i)->isClear(_plusTime) == true)
								{
									std::cout << "wanted_view1" << std::endl;
									wantedViewFound = true;
									break;
								}
								else
								{
									std::cout << "notwanted_view1" << std::endl;
									wantedViewFound = false;
								}
							}
							else
							{
								std::cout << "wanted_view2" << std::endl;
								wantedViewFound = true;
								break;
							}
						}
					}
				}
			}

			if(wantedViewFound == false)
			{
				//std::cout << "notwanted_view1" << std::endl;
				wantedTileFound = false;
				return false;
			}
		}

		return wantedTileFound;
	}
	
	NodePath* findTile(NodeMap& _map, PM::Position _start, int _options, FindProperties _properties, int _plusTime)
	{
		// initialization
		bool wantedTileFound = false;
		PM::PathFinding::NodeHeap openNodesHeap;

		// auxiliary variables
		Node* node = NULL;
		Node* neighbourNode = NULL;
		Position position(0,0);
		int elapsedTime = 0;
		int timeToExplode = 0;
		bool ignoredTile = false;

		std::vector< PM::TileType > ignoreTypes;

		if(_options & IGNORE_WALLS) ignoreTypes.push_back(WALL);
		if(_options & IGNORE_UWALLS) ignoreTypes.push_back(UWALL);

		_map.clear();

		node = _map.getNode(_start);
		node->open(NULL, _map.getTileMap().getTile(_start), _plusTime);
		openNodesHeap.insert(node);
		_map.getNodesForClear().push_back(node);

		if(_options & CONSIDER_BOMBS)
		{
			std::cout << "plus " << _plusTime << std::endl;
			std::cout << node->getTileStateOnGoThrough() << " " << PM::Global::AISlowDown << " " << node->isTileClearOnGoThrough() << std::endl;
			if(_plusTime != 0)
			{
				//std::cin.get();
			}

			if(node->getTileStateOnGoThrough() < 0 /*= PM::Global::AISlowDown*/ && node->isTileClearOnGoThrough() == false)
			{
				return NULL;
			}
		}

		// check if wanted tile is on the start position
		wantedTileFound = checkTile(_map, node->getTile(), _options, _properties);
		
		while(openNodesHeap.empty() != true && wantedTileFound != true)
		{
			node = openNodesHeap.getMin();
			node->finish();

			if(_options & MAX_LENGTH)
			{
				if(node->getDistance() == _properties.maxLength)
				{
					return NULL;
				}
			}

			elapsedTime = node->getDistance() * PM::Global::AISlowDown;

			std::cout << node->getTile()->getPosition().x << " " << node->getTile()->getPosition().y << std::endl;
			for(int i = 0; i < 4; i++)
			{
				position = node->getTile()->getPosition().move(i + 1);
				std::cout << i << ": " <<  position.x << " " << position.y << std::endl;

				neighbourNode = _map.getNode(position);

				if(neighbourNode->getType() == C)
				{
						neighbourNode->open(node, _map.getTileMap().getTile(position), elapsedTime + _plusTime);

						// check if this is the wanted tile (must be a closed one)
						wantedTileFound = checkTile(_map, neighbourNode->getTile(), _options, _properties, _plusTime);
						
						// check if this is the tile of ignored type
						ignoredTile = false;
						for(int j = 0; j < ignoreTypes.size(); j++)
						{
							//std::cout << "ig" << std::endl;
							if(neighbourNode->getTile()->getType() == ignoreTypes[j])
							{
								//std::cout << "ig - yes" << std::endl;
								ignoredTile = true;
							}
						}

						if(_options & IGNORE_POSITION)
						{
							if(neighbourNode->getTile()->getPosition() == _properties.ignorePosition)
							{
								ignoredTile = true;
							}
						}

						if(_options & CONSIDER_BOMBS)
						{
							if(neighbourNode->getTile()->getType() == WALL && node->getTile()->isClear(_plusTime) == false)
							{
								timeToExplode = node->getTile()->getState(_plusTime) + 1;

								if(timeToExplode - elapsedTime > -PM::Global::bombTimeToVaporize)
								{
									wantedTileFound = false;
									ignoredTile = true;
								}
							}
							else if(neighbourNode->getTile()->isClear(_plusTime) == false)
							{
								timeToExplode = neighbourNode->getTile()->getState(_plusTime) + 1;

								if(timeToExplode - elapsedTime <= PM::Global::AISlowDown && timeToExplode - elapsedTime > -PM::Global::bombTimeToVaporize)
								{
									if(node->getTile()->isClear(_plusTime) == false)
									{
										timeToExplode = node->getTile()->getState(_plusTime) + 1;
									
										if(timeToExplode - elapsedTime <= (PM::Global::AISlowDown)); // * waitMoveCount))
										{
											ignoredTile = true;
										}
									}

									if(ignoredTile == false)
									{
										neighbourNode->setDistance(neighbourNode->getDistance() + 1);
									}
								}
							}
						}

						std::cout << wantedTileFound << std::endl;
						// skip, if this tile is on the ignore list
						if(ignoredTile == true && wantedTileFound == false)
						{
							std::cout << "ignore" << std::endl;
							neighbourNode->clear();
							//continue;
						}
						else
						{
							std::cout << "closed: " << neighbourNode->getDistance() << std::endl;
							openNodesHeap.insert(neighbourNode);
							_map.getNodesForClear().push_back(neighbourNode);

							if(wantedTileFound == true)
							{
								std::cout << "wanted" << std::endl;
								node = neighbourNode;
								break;
							}
						}

				}
				else if(neighbourNode->getType() == O)
				{
						std::cout << "opened: " << neighbourNode->getDistance() << std::endl;
						if(neighbourNode->getDistance() > node->getDistance() + tileCost(neighbourNode->getTile()->getType()))
						{
							neighbourNode->setDistance(node->getDistance() + tileCost(neighbourNode->getTile()->getType()));
							neighbourNode->setParent(node);
							openNodesHeap.update(neighbourNode);
							std::cout << "change: " << neighbourNode->getDistance() << std::endl;
						}
				}
				std::cout << "";
				//std::cin.get();
			}
		}

		if(wantedTileFound == true)
		{
			// build the path
			NodePath* path = new NodePath(_map, _options, _properties);

			while(node->getParent() != NULL)
			{
				path->addFront(node);
				node = node->getParent();
			}
		
			return path;
		}
		else
		{
			// path haven't been found
			return NULL;
		}
	}

	}
}
