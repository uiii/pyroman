#include "../map.h"

namespace PM
{
	namespace PathFinding
	{
// -- Node functions -- //
	void Node::clear()
	{
		this->distance =-1;
		this->heuristic = 0;
		this->type = C;
		this->parent = NULL;
		this->tileClearOnGoThrough = true;
		this->tileStateOnGoThrough = 0;
		this->tile = NULL;
	}

	void Node::open(Node* _parent, PM::Tile* _tile, int _plusTime)
	{
		this->tile = _tile;
		
		if(_parent == NULL)
		{
			this->distance = 0;
		}
		else
		{
			this->parent = _parent;
			this->distance = this->parent->getDistance() + tileCost(this->tile->getType());
		}

		this->type = O;

		this->tileClearOnGoThrough = this->tile->isClear(_plusTime);
		this->tileStateOnGoThrough = this->tile->getState(_plusTime);
	}
	
// -- NodePath functions -- //
	void NodePath::addFront(PM::PathFinding::Node* _node)
	{
		path.push_back(_node->getTile());
		tileClear.push_back(_node->isTileClearOnGoThrough());
		tileState.push_back(_node->getTileStateOnGoThrough());
	}

	void NodePath::addBack(PM::PathFinding::Node* _node)
	{
		path.insert(path.begin(), _node->getTile());
		tileClear.insert(tileClear.begin(), _node->isTileClearOnGoThrough());
		tileState.insert(tileState.begin(), _node->getTileStateOnGoThrough());
	}

	PM::Tile* NodePath::next()
	{
		PM::Tile* tile = NULL;

		std::cout << this->path.size() << std::endl;

		if(path.size() > 0)
		{
			tile = this->path.back();
			if(tile->isClear() != this->tileClear.back())
			{
				this->currentTileChanged = true;
			}
			else
			{
				this->currentTileChanged = false;
			}

			this->path.pop_back();
			this->tileClear.pop_back();
		}

		if(tile == NULL)
		{
			std::cout << "nulll" << std::endl;
		}

		return tile;
	}

	bool NodePath::checkEnd()
	{
		return PM::PathFinding::checkTile(this->map, this->path.front(), this->findOptions, this->findProperties);
	}

// -- NodeMap functions -- //
	NodeMap::NodeMap(PM::Map& _map):
		tileMap(_map)
	{
		this->nodes.resize(PM::Global::screenWidth / 20, std::vector< Node >(PM::Global::screenHeight / 20));
	}

	void NodeMap::clear()
	{
		for(int i = 0; i < nodesForClear.size(); i++)
		{
			nodesForClear[i]->clear();
		}

		nodesForClear.clear();
	}

	NodePath* NodeMap::findPosition(PM::Position _start, PM::Position _end)
	{
		// TODO dodelat aby fungovalo COSIDER_BOMBS !!!!
		FindProperties findProperties;
		findProperties.position = _end;

		PM::PathFinding::findTile(*this, _start, TILE_POSITION | IGNORE_WALLS | IGNORE_UWALLS | CONSIDER_BOMBS, findProperties);
	}
	
	NodePath* NodeMap::findWall(PM::Position _start, int _range, bool considerCurrentPosition)
	{
		FindProperties properties;
		int options = 0;
		
		if(_range == 0)
		{
			properties.type = WALL;

			options = TILE_TYPE | TILE_IS_CLEAR;
		}
		else
		{
			properties.type = PASSAGE;
			properties.viewDistance = _range;
			properties.viewType = WALL;
		
			options = TILE_TYPE | TILE_IS_CLEAR | TILE_VIEW_TYPE | TILE_VIEW_IS_CLEAR;
		}

		if(considerCurrentPosition == false)
		{
			options = options | TILE_NOT_POSITION;
			properties.notPosition = _start;
		}

		options = options | IGNORE_WALLS | IGNORE_UWALLS | CONSIDER_BOMBS;

		return PM::PathFinding::findTile(*this, _start, options, properties);
	}
	
	NodePath* NodeMap::findPathToImprovement(PM::Position _start, PM::Position _end, int _maxLength)
	{
		FindProperties properties;
		int options = 0;
		
		properties.position = _end;
		
		options = TILE_POSITION | TILE_IS_CLEAR | IGNORE_WALLS | IGNORE_UWALLS | CONSIDER_BOMBS;

		if(_maxLength != -1)
		{
			options = options | MAX_LENGTH;
			properties.maxLength = _maxLength;
		}

		return PM::PathFinding::findTile(*this, _start, options, properties);
	}
	
	NodePath* NodeMap::findShelter(PM::Position _start, int _plusTime)
	{
		FindProperties properties;
		properties.type = PASSAGE;

 		int options = TILE_TYPE | TILE_IS_CLEAR | IGNORE_WALLS | IGNORE_UWALLS | CONSIDER_BOMBS;
		
		return PM::PathFinding::findTile(*this, _start, options, properties, _plusTime);
	}
	
	NodePath* NodeMap::findTileCloserToPlayer(PM::Position _start, int _range, bool considerCurrentPosition)
	{
		PM::Position playerPosition = this->tileMap.getPlayer()->getPosition();
		int xDistance = abs(_start.x - playerPosition.x) / 2;
		int yDistance = abs(_start.y - playerPosition.y) / 2;

		if(xDistance < _range)
		{
			xDistance = _range;
		}

		if(yDistance < _range)
		{
			yDistance = _range;
		}

		FindProperties properties;
		properties.type = PASSAGE | WALL;
		properties.playerXDistance = xDistance;
		properties.playerYDistance = yDistance;

		int options = TILE_TYPE | PLAYER_DISTANCE | IGNORE_UWALLS | CONSIDER_BOMBS;
		
		if(considerCurrentPosition == false)
		{
			options = options | TILE_NOT_POSITION;
			properties.notPosition = _start;
		}

		return PM::PathFinding::findTile(*this, _start, options, properties);
	}
	
	NodePath* NodeMap::findTileToBombPosition(PM::Position _start, PM::Position _target, int _range, bool _considerCurrentPosition)
	{
		FindProperties properties;
		properties.type = PASSAGE;
		properties.viewPosition = _target;
		properties.viewDistance = _range;

		int options = TILE_TYPE | TILE_IS_CLEAR | TILE_VIEW_POSITION | IGNORE_WALLS | IGNORE_UWALLS | CONSIDER_BOMBS;

		if(_considerCurrentPosition == false)
		{
			options = options | TILE_NOT_POSITION;

			properties.notPosition = _start;
		}

		return PM::PathFinding::findTile(*this, _start, options, properties);
	}
	
	NodePath* NodeMap::findTileToBombPlayer(PM::Position _start, int _range, bool _considerCurrentPosition)
	{
		FindProperties properties;
		properties.type = PASSAGE | WALL;
		properties.playerXDistance = _range;
		properties.playerYDistance = _range;

		int options = TILE_TYPE | TILE_IS_CLEAR | TILE_TO_BOMB_PLAYER | PLAYER_DISTANCE | IGNORE_UWALLS | CONSIDER_BOMBS;

		if(_considerCurrentPosition == false)
		{
			options = options | TILE_NOT_POSITION;

			properties.notPosition = _start;
		}

		return PM::PathFinding::findTile(*this, _start, options, properties);
	}

	NodePath* NodeMap::findTheSame(PM::Position _start, NodePath* _path, PM::Tile* _ignoreTile)
	{
		PM::PathFinding::FindProperties properties = _path->getProperties();
		int options = _path->getOptions();

		if(_ignoreTile != NULL)
		{
			options = options | IGNORE_POSITION;
			properties.ignorePosition = _ignoreTile->getPosition();
		}

		return PM::PathFinding::findTile(*this, _start, _path->getOptions(), _path->getProperties());
	}

	bool NodeMap::checkCurrentPosition(PM::Position _position, int _options, PM::PathFinding::FindProperties _properties)
	{
		return PM::PathFinding::checkTile(*this, this->tileMap.getTile(_position), _options, _properties);
	}

	}
}
