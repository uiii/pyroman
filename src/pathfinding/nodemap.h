#ifndef PM_NODEMAP_H
#define PM_NODEMAP_H

#include "../global.h"

#include "pathfinding.h"
#include "nodeheap.h"

namespace PM
{
	class Tile; // forward declaration
	class Map; // forward declaration

	namespace PathFinding
	{
	
	enum NodeType
	{
		C, // closed
		O, // opened
		F  // finished
	};

	class Node
	{
	public:
		Node()
		{
			this->clear();
		}

		void clear();

		void open(Node* _parent, PM::Tile* _tile, int plusTime = 0);
		void finish() { this->type = F; }

		void setDistance(int _distance) { this->distance = _distance; }
		void setParent(Node* _parent) { this->parent = _parent; }

		int getDistance() const { return this->distance; }
		int getHeuristic() const { return this->heuristic; }
		
		// returns a sum of distance and heuristic
		int getTotal() const { return this->distance + this->heuristic; }

		NodeType getType() const { return this->type; }
		Node* getParent() const { return this->parent; }

		bool isTileClearOnGoThrough() { return this->tileClearOnGoThrough; }
		int getTileStateOnGoThrough() { return this->tileStateOnGoThrough; }

		PM::Tile* getTile() const { return this->tile; }

	private:
		int distance;
		int heuristic;

		NodeType type;
		Node* parent;

		bool tileClearOnGoThrough;
		int tileStateOnGoThrough;
		PM::Tile* tile;

	// for heap use only
	public:
		int heapIndex;
	};

	class NodePath
	{
	public:
		NodePath(NodeMap& _map, int _options, PM::PathFinding::FindProperties _properties):
			map(_map),
			findOptions(_options),
			findProperties(_properties)
		{}

		int getOptions() { return this->findOptions; }
		PM::PathFinding::FindProperties getProperties() { return this->findProperties; }

		void addFront(PM::PathFinding::Node* _node);
		void addBack(PM::PathFinding::Node* _node);

		PM::Tile* next();

		bool isCurrentTileChanged()	{ return this->currentTileChanged; }

		bool checkEnd();

		bool empty() { return this->path.empty(); }

		// for dubugging only
		PM::Tile* getEnd()
		{
			if(this->path.size() > 0) return this->path.front();
			else return NULL;
		}

		void show(SDL_Surface* _surface)
		{
			int r = path.size() % 256;
			int g = (path.size() * r) % 256;
			int b = (path.size() * g) % 256;

			SDL_Surface* surface = PM::createEmpySurface(20, 20);
			Uint32 color;

			int x, y;

			std::cout << "size: " << path.size() << std::endl;
			
			for(int i = path.size(); i > 0; i--)
			{
				std::cout << "i: " << i << ": ";
				x = path[i-1]->getPosition().x * 20;
				y = path[i-1]->getPosition().y * 20;
				std::cout << x / 20 << " " << y / 20 << std::endl;
				
				color = SDL_MapRGBA(surface->format, r, g, b, 200 - i*(200/path.size()));
				SDL_FillRect(surface, NULL, color);
				applySurface(x, y, surface, _surface);
			}

			SDL_FreeSurface(surface);
		}

	private:
		std::vector< PM::Tile* > path;
		std::vector< bool > tileClear;
		std::vector< int > tileState;

		bool currentTileChanged;

		NodeMap& map;

		int findOptions;
		PM::PathFinding::FindProperties findProperties;
	};

	class NodeMap
	{
	public:
		NodeMap(PM::Map& map);

		Node* getNode(PM::Position _position) { return &nodes[_position.x][_position.y]; }
		PM::Map& getTileMap() { return this->tileMap; }
		std::vector< Node* >& getNodesForClear() { return this->nodesForClear; }

		std::vector< Node >& operator[](int _number) { return nodes[_number]; }

		void clear();

		// finds the shortest path to the closest tile of specified type
		NodePath* findPosition(PM::Position start, PM::Position end);

		// finds the shortest path to the closest tile of specified type
		NodePath* findWall(PM::Position start, int range = 0, bool considerCurrentPosition = true);

		NodePath* findPathToImprovement(PM::Position start, PM::Position end, int maxLength = -1);

		// finds the shortest path to the closest tile of specified type
		NodePath* findShelter(PM::Position start, int plusTime = 0);

		NodePath* findTileCloserToPlayer(PM::Position start, int range = 0, bool considerCurrentPosition = true);

		NodePath* findTileToBombPosition(PM::Position start, PM::Position target, int range, bool considerCurrentPosition = true);
		NodePath* findTileToBombPlayer(PM::Position start, int range, bool considerCurrentPosition = true);

		NodePath* findTheSame(PM::Position start, NodePath* path, PM::Tile* ignoreTile = NULL);

		bool checkCurrentPosition(PM::Position position, int options, PM::PathFinding::FindProperties properties);

	private:
		std::vector< std::vector< Node > > nodes;

		std::vector< Node* > nodesForClear;

		PM::Map& tileMap;
	};
 	
	}
}
#endif
