#include "nodeheap.h"

#include "nodemap.h"
#include "pathfinding.h"

namespace PM
{
	namespace PathFinding
	{
	
	void NodeHeap::insert(PM::PathFinding::Node* _node)
	{	
		this->heapNodes.push_back(_node);
		_node->heapIndex = heapNodes.size() - 1;

		std::cout << "v--- insert ----v" << std::endl;
		this->update(_node);
		std::cout << "^--- insert ----^" << std::endl;
	}

	void NodeHeap::update(PM::PathFinding::Node* _node)
	{
		int index = _node->heapIndex;
		// compare with parent
		while(index != 0)
		{
			if(heapNodes[index]->getTotal() < heapNodes[(index - 1) / 2]->getTotal())
			{
				PM::swap(heapNodes[index]->heapIndex, heapNodes[(index - 1) / 2]->heapIndex); // first swap heapIndexes of nodes
				PM::swap(heapNodes[index], heapNodes[(index - 1) / 2]); // second swap nodes themselves
			}
			else
			{
				break;
			}
			
			index = (index - 1) / 2;
		}

		// compare with childs
		int childIndex;
		while((index * 2) + 1 <= heapNodes.size() - 1)
		{
			std::cout << "update down" << std::endl;
			childIndex = index;

			std::cout << "index: " << index << std::endl;
			std::cout << "heapNodes[index]: " << heapNodes[index] << std::endl;
			std::cout << "heapNodes[index*2+1]: " << heapNodes[(index * 2) + 1] << std::endl;
			if(heapNodes[index]->getTotal() > heapNodes[(index * 2) + 1]->getTotal())
			{
				std::cout << "child first" << std::endl;
				childIndex = (index * 2) + 1;
			}
			
			if((index * 2) + 2 <= heapNodes.size() - 1)
			{
				if(heapNodes[(index * 2) + 2]->getTotal() < heapNodes[childIndex]->getTotal())
				{
					childIndex = (index * 2) + 2;
				}
			}

			if(childIndex != index)
			{
				std::cout << "swap" << std::endl;
				PM::swap(heapNodes[index]->heapIndex, heapNodes[childIndex]->heapIndex);
				PM::swap(heapNodes[index], heapNodes[childIndex]);
			}
			else
			{
				break;
			}

			index = childIndex;
		}
	}

	PM::PathFinding::Node* NodeHeap::getMin()
	{
		if(this->heapNodes.size() > 0)
		{
			PM::PathFinding::Node* minimum = this->heapNodes[0];

			this->heapNodes[0] = this->heapNodes[this->heapNodes.size() - 1];
			this->heapNodes[0]->heapIndex = 0;
			heapNodes.pop_back();
			if(heapNodes.size() != 0)
			{
				std::cout << "update" << std::endl;
				this->update(heapNodes[0]);
			}

			return minimum;
		}
		else
		{
			return NULL;
		}
	}

	}
}
