#ifndef PM_NODEHEAP_H
#define PM_NODEHEAP_H

#include <vector>

namespace PM
{
	namespace PathFinding
	{

	class Node; // forward declaration

	// item of NodeHeap
	/*struct HeapContainer
	{
	public:
		HeapContainer(PM::PathFinding::Node* _node)
		{
			this->setNode(_node);
		}

		void setNode(PM::PathFinding::Node* _node)
		{
			this->node = _node;
			this->node->setHeapContainer(this);
		}

		PM::PathFinding::Node* getNode()
		{
			return this->node;
		}

		int index;

	private:
		PM::PathFinding::Node* node;
	};*/

	// heap of HeapNode items
	class NodeHeap
	{
	public:
		NodeHeap()
		{}

		~NodeHeap()
		{
			heapNodes.clear();
		}

		void insert(PM::PathFinding::Node* _node);
		void update(PM::PathFinding::Node* _node);

		PM::PathFinding::Node* getMin();

		bool empty()
		{
			if(heapNodes.size() == 0)
			{
				return true;
			}
			else
			{
				return false;
			}
		}

	private:
		std::vector< PM::PathFinding::Node* > heapNodes;
	};

	}
}

#endif
