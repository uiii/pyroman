#ifndef PM_MAP_H
#define PM_MAP_H

#include <string>
#include <vector>

#include "global.h"
#include "exception.h"

#include "tile.h"
#include "character.h"
#include "bomb.h"

namespace PM
{

class Map
{
public:
	Map(const std::string& mapSourceFile);
	~Map();

	int build();
	void setViews();

	PM::Tile* getTile(PM::Position _position) { return tiles[_position.x][_position.y]; }
	PM::Character* getPlayer() { return this->player; }
	
	int getWallsCount() { return this->wallsCount; }
	int getImprovementsCount() { return this->improvementsCount; }

	int getInitialWallsCount() { return this->initialWallsCount; }
	int getInitialImprovementsCount() { return this->initialImprovementsCount; }

	void placeCharacter(PM::Character* character);
	void placeBomb(PM::Bomb* bomb);

	void control(); // control the happening on the map
	void show(SDL_Surface* surface);

	sigc::signal<void, PM::Character*> signal_addCharacter;
	sigc::signal<void, PM::Improvement*> signal_improvementFound;
	sigc::signal<void, PM::Improvement*> signal_improvementRemoved;

private:
	void setScreenSize();

	void improvementFound(PM::Improvement* _improvement);
	void improvementRemoved(PM::Improvement* _improvement);

	void wallDestroyed() { this->wallsCount--; }

	std::vector< std::vector< PM::Tile* > > tiles;

	PM::Character* player;

	int wallsCount;
	int improvementsCount;

	int initialWallsCount;
	int initialImprovementsCount;

	int width;
	int height;

	std::string sourceFile;
};

}

#endif
