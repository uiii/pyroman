#include "bomb.h"

#include "tile.h"
#include "character.h"

namespace PM
{

Bomb::Bomb(PM::Character* _owner, int _range):
	owner(_owner),
	position(_owner->getPosition()),
	range(_range),
	timeToExplode(PM::Global::bombTimeToExplode),
	timeToVaporize(PM::Global::bombTimeToVaporize),
	exploding(false)
{
	this->timer.start("explode"); // start countdown to the explosion

	// set texture to bomb
	this->texture.x = 100;
	this->texture.y = 0;
	this->texture.w = 20;
	this->texture.h = 20;
}

void Bomb::addTile(PM::Tile* _tile)
{
	tiles.push_back(_tile);
}

void Bomb::control()
{
	if(this->exploding == true)
	{
		if(this->timer.getTicks("vaporize") >= this->timeToVaporize)
		{
			this->vaporize();
		}
	}
	else
	{
		std::cout << "explode1 " << timer.getTicks("explode") << std::endl;
		if(this->timer.getTicks("explode") >= this->timeToExplode)
		{
			std::cout << "explode2 " << timer.getTicks("explode") << std::endl;
			this->explode();
		}
	}
}

void Bomb::explode()
{
	this->texture.x += 20; // change texture to explosion
	
	this->exploding = true; // the bomb has exploded

	this->timer.stop("explode");
	this->timer.start("vaporize"); // start countdown to vaporization of the explosion

	for(int i = 0; i < this->tiles.size(); i++)
	{
		tiles[i]->destroyRequest();
	}
}

void Bomb::vaporize()
{
	this->timer.stop("vaporize");

	for(int i = 0; i < this->tiles.size(); i++)
	{
		tiles[i]->removeBomb(this);
	}

	if(this->owner != NULL)
	{
		this->owner->bombVaporize(this);
	}
	
	this->signal_vaporize.emit(this);

	delete this;
}

void Bomb::ownerDie()
{
	this->owner = NULL;
}

int Bomb::getRemainingTime()
{
	if(this->exploding == true)
	{
		return -this->timer.getTicks("vaporize");
	}
	else
	{
		return this->timeToExplode - this->timer.getTicks("explode");
	}
}

void Bomb::show(SDL_Surface* _surface, PM::Position _position)
{
	SDL_Rect target = _position.getRect();
	target.x *= 20;
	target.y *= 20;

	applySurface(&this->texture, PM::Global::textureSource, &target, _surface);
}

}
