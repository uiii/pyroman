#ifndef PM_TILE_H
#define PM_TILE_H

#include <vector>

#include "global.h"

namespace PM
{

class Map; // forward declaration
class Character; // forward declaration
class Bomb; // forward declaration

enum ImprovementType
{
	BOMB = 1,
	RANGE = 2
};

struct Improvement
{
	Improvement(ImprovementType _type, PM::Position _position):
		type(_type),
		position(_position)
	{}

	ImprovementType type;
	PM::Position position;

	void show(SDL_Surface* _surface)
	{
		SDL_Rect texture;
		SDL_Rect target;

		texture.w = 20;
		texture.h = 20;

		switch(type)
		{
			case BOMB:
				texture.x = 140;
				texture.y = 0;
				break;
			case RANGE:
				texture.x = 140;
				texture.y = 20;
				break;
		}

		target.x = this->position.x * 20;
		target.y = this->position.y * 20;

		PM::applySurface(&texture, PM::Global::textureSource, &target, _surface);
	}
};

enum TileType
{
	PASSAGE = 1, // free passage
	WALL = 2, 	 // destuctable wall
	UWALL = 4 	 // indestructable wall
};

class Tile
{
public:
	Tile(PM::Map& map, int x, int y, TileType type = PASSAGE);

	int setImprovement();
	bool hasImprovement();

	void setView(int _number, PM::Tile* _tile) { this->view[_number] = _tile; }
	void setViewDistance(int _number, int _distance) { this->viewDistance[_number] = _distance; }

	PM::Tile* getView(int _number) { return this->view[_number]; }
	int getViewDistance(int _number) { return this->viewDistance[_number]; }

	TileType getType() { return this->type; }
	int getState(int plusTime = 0);
	bool isClear(int plusTime = 0);
	PM::Position getPosition() { return this->position; }

	void placeCharacter(PM::Character* _character);
	void removeCharacter(PM::Character* _character);
	void placeBomb(PM::Bomb* _bomb);
	void removeBomb(PM::Bomb* _bomb);

	void control(); // control happening on this tile

	void destroyRequest();

	void show(SDL_Surface* surface);

	sigc::signal<void, PM::Improvement*> signal_improvementFound;
	sigc::signal<void, PM::Improvement*> signal_improvementRemoved;
	sigc::signal<void> signal_wallDestroyed;

private:
	void setTexture();
	void setState();
	
	PM::Position position;

	TileType type;

	// current state of this tile
	// positive values (includes zero) means approximate remaining time to explode
	// negative values means approximate elapsed time from explosion to vaporize
	int currentState;

	// clear is true if there are no bombs on this tile
	// currentState is set to zero
	bool clear;

	// what tiles (not passages) did this tile see
	// directions are in sequence 0,1,2,3 - up, down, left, right
	std::vector< PM::Tile* > view;
	// and in what distances
	std::vector< int > viewDistance;

	std::vector< PM::Character* > characters;
	std::vector< PM::Bomb* > bombs;

	SDL_Rect texture;

	PM::Map& map;

	PM::Improvement* improvement;
};

}

#endif
