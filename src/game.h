#ifndef PM_GAME_H
#define PM_GAME_H

#include <string>
#include <vector>

#include "global.h"
#include "map.h"
#include "timer.h"

#include "character.h"
#include "bomb.h"

namespace PM
{

class Game
{
public:
	Game(const std::string& mapSourceFile, const std::string& textureSourceFile);
	~Game();

	void init();
	void deleteAll();

	void restart(); // restart the game

	void mainLoop();

	void addCharacter(PM::Character* character);
	void removeCharacter(PM::Character* character);

	void addBomb(PM::Bomb* bomb);
	void removeBomb(PM::Bomb* bomb);

	void addImprovement(PM::Improvement* improvement);
	void removeImprovement(PM::Improvement* improvement);

	void decreaseEnemyCount();

	void setVictory() { this->victory = true; }
	void setLoss() { this->loss = true; }

	void showVerdict();

private:
	std::string mapSourceFile;

	SDL_Surface* screen;

	PM::Timer fps;

	PM::Map* map;
	std::vector< PM::Character* > characters;
	std::vector< PM::Bomb* > bombs;
	std::vector< PM::Improvement* > improvements;

	int enemyCount;

	bool pause;

	bool victory;
	bool loss;
};

}

#endif
