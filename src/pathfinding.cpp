#include "pathfinding.h"

namespace PM
{
	namespace PathFinding
	{
	
	NodeMap::NodeMap(PM::Map& _map):
		tileMap(_map)
	{
		this->nodes.resize(PM::Global::SCREEN_WIDTH, std::vector< Node >(PM::Global::SCREEN_HEIGHT));
	}

	void openNode(PM::Position _position)
	{
		int x = _position.x;
		int y = _position.y;

		nodes[x][y].type = 0;
	}

	void NodeMap::clear()
	{
		for(int i = 0; i < nodesForClear.size(); i++)
		{
			nodesForClear[i]->clear();
		}
	}

	findPath(const PM::Position* _start, const PM::Position* _end, bool _avoidBombs)
	{
	}

	NodePath* NodeMap::findWall(const PM::Position* _start, bool _avoidBombs)
	{
		Node* node = NULL;
		Node* node2 = NULL;
		PM::Position position;
		
		bool endTileFound = false;

		node = this->getNode(_start); // starting node
		node->open(NULL, this->tileMap.getTile(_start));
		openNodes.add(node);
		nodesForClear.push_back(node);

		// main loop of the map passing
		while(openNodes.empty() != true)
		{
			node = openNodes.getMin(); // get and remove the minimal node from heap of opened nodes
			node->finish();
				
			if(node->getTile()->getType() == WALL) // end tile type has been found
			{
				endTileFound = true;
				break;
			}

			// set position of the first neighbour of the node
			position.x = 1;
			position.y = 0;
			for(int i = 0, int k = -1; i < 4; i++, k = k * -1)
			{
				node2 = this->getNode(position);

				if(this->tileMap.getTile(position)->getType() != UWALL) // ignore unwalkable tiles
				{
					if(node2->getType() == C) // neighbour is closed
					{
						node2->open(node, this->tileMap.getTile(position));
						openNodes.add(node2);
					}
					else if(node2->getType() == O) // neighbour is opened
					{
						if(node2->getDistance() > node->getDistance() + 1)
						{
							node2->setDistance(node->getDistance() + 1);
							node2->setParent(node);
							openNodes.check(node2);
						}
					}
					
					nodesForClear.push_back(node2);
				}
				
				// get position of the next neighbour
				PM::swap(position.x,position.y);
				position.y *= k;
			}
		}

		if(endTileFound = true)
		{
			// build the path
			NodePath* path = new NodePath();
			path->add(node);

			while(node->getParent() != NULL)
			{
				path->add(node->getParent());
				node = node->getParent();
			}
			
			return path;
		}
		else
		{
			// path haven't been found
			return NULL;
		}
	}

	}
}
