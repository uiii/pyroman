#ifndef PM_PATHFINDING_H
#define PM_PATHFINDING_H

#include "global.h"

#include "map.h"
#include "tile.h"

namespace PM
{
	namespace PathFinding

	enum NodeType
	{
		C, // closed
		O, // opened
		F  // finished
	};

	class Node:
	{
	public:
		Node()
		{
			this->clear();
		}

		void clear()
		{
			this->distance =-1,
			this->heuristic = 0,
			this->type = C,
			this->parent = NULL,
			this->tile = NULL
		}

		void open(Node* _parent, Tile* _tile)
		{
			if(_parent == NULL)
			{
				this->distance = 0;
				tile = _tile;
			}
			else
			{
				this->parent = _parent;
				this->distance = this->parent->getDistance() + 1;
			}
		}
		void finish()
		{
			this->type = F;
		}

		int getDistance() { return this->distance; }
		int getTotal() { return this->distance + this->heuristic; } // returns a sum of distance and heuristic

	private:
		int distance;
		int heuristic;

		NodeType type;
		Node* parent;

		PM::Tile* tile;
	};

	class NodePath
	{
	public:
		NodePath():
			actual(0)
		{}

		void add(Node* _node)
		{
			path.push_back(_node);
		}

		PM::Tile next()
		{
			PM::Tile* tile = NULL;

			if(actual < path.size())
			{
				tile = path[aktual]->getTile();
				this->actual++;
			}

			return tile;
		}

	private:
		std::vector< Node* > path;

		int actual;
	}

	class NodeMap
	{
	public:
		Map(PM::Map& map);

		void openNode(PM::Position position);

		Node& getNode(PM::Position _position) { return nodes[_position.x][_position.y]; }
		std::vector< Node >& operator[](int _number) { return nodes[_number]; }

		void clear();

		// finds the shortest path from the start to the end
		NodePath* findPath(const PM::Position* start, const PM::Position* end, bool avoidBombs = false);
 	
		// finds the shortest path to the closest tile of specified type
		NodePath* findWall(const PM::Position* start, bool avoidBombs = false);

	private:
		std::vector< std::vector< Node > > nodes;

		PM::OpenNodeHeap openNodes;

		std::vector< Node* > nodesForClear;

		PM::Map& tileMap;
	};
 	
	}
}
#endif
