#include "game.h"

#include <iostream>

namespace PM
{

Game::Game(const std::string& _mapSourceFile, const std::string& _textureSourceFile):
	mapSourceFile(_mapSourceFile),
	map(NULL),
	enemyCount(0),
	pause(false),
	victory(false),
	loss(false)
{
	PM::Global::textureSource = PM::loadImage(_textureSourceFile);
	PM::Global::font = TTF_OpenFont("coah.ttf", 32);

	this->init();
}

Game::~Game()
{
	this->deleteAll();
}

void Game::init()
{
	this->map = new PM::Map(this->mapSourceFile);

	this->map->signal_addCharacter.connect(sigc::mem_fun(*this, &PM::Game::addCharacter));
	this->map->signal_improvementFound.connect(sigc::mem_fun(*this, &PM::Game::addImprovement));
	this->map->signal_improvementRemoved.connect(sigc::mem_fun(*this, &PM::Game::removeImprovement));

	this->enemyCount = this->map->build();

	this->screen = SDL_SetVideoMode(PM::Global::screenWidth, PM::Global::screenHeight, PM::Global::SCREEN_BPP, SDL_SWSURFACE);

	if(this->screen == NULL)
	{
		throw PM::Exception("Cannot get the screen surface");
	}
}

void Game::deleteAll()
{
	delete this->map;

	for(int i = 0; i < this->characters.size(); i++)
	{
		delete this->characters[i];
	}

	this->characters.clear();

	for(int i = 0; i < this->bombs.size(); i++)
	{
		delete this->bombs[i];
	}

	this->bombs.clear();

	for(int i = 0; i < this->improvements.size(); i++)
	{
		delete this->improvements[i];
	}

	this->improvements.clear();
}

void Game::restart()
{
	this->deleteAll();

	this->enemyCount = 0;
	this->victory = false;
	this->loss = false;
	
	this->init();
}

void Game::addCharacter(PM::Character* _character)
{
	this->characters.push_back(_character);
	_character->signal_addBomb.connect(sigc::mem_fun(*this, &PM::Game::addBomb));
	_character->signal_die.connect(sigc::mem_fun(*this, &PM::Game::removeCharacter));
	_character->signal_pyromanDie.connect(sigc::mem_fun(*this, &PM::Game::setLoss));
	_character->signal_pyrobotDie.connect(sigc::mem_fun(*this, &PM::Game::decreaseEnemyCount));

	map->placeCharacter(_character);
}

void Game::removeCharacter(PM::Character* _character)
{
	std::vector< PM::Character* >::iterator it;
	for(it = this->characters.begin(); it < this->characters.end(); it++)
	{
		if(*it == _character)
		{
			this->characters.erase(it);
			break;
		}
	}
}

void Game::addBomb(PM::Bomb* _bomb)
{
	this->bombs.push_back(_bomb);
	_bomb->signal_vaporize.connect(sigc::mem_fun(*this, &PM::Game::removeBomb));

	map->placeBomb(_bomb);
}

void Game::removeBomb(PM::Bomb* _bomb)
{
	std::vector< PM::Bomb* >::iterator it;
	for(it = this->bombs.begin(); it < this->bombs.end(); it++)
	{
		if(*it == _bomb)
		{
			this->bombs.erase(it);
			break;
		}
	}
}

void Game::addImprovement(PM::Improvement* _improvement)
{
	this->improvements.push_back(_improvement);
}

void Game::removeImprovement(PM::Improvement* _improvement)
{
	std::vector< PM::Improvement* >::iterator it;
	for(it = this->improvements.begin(); it < this->improvements.end(); it++)
	{
		if(*it == _improvement)
		{
			this->improvements.erase(it);
			break;
		}
	}
			std::cout << "ted - game" << std::endl;
}

void Game::decreaseEnemyCount()
{
	this->enemyCount--;
	if(enemyCount == 0) this->setVictory();
}

void Game::showVerdict()
{
	std::string verdict;
	SDL_Surface* surface = PM::createEmpySurface(PM::Global::screenWidth, PM::Global::screenHeight);
	Uint32 color;

	if(this->victory == true)
	{
		verdict = "Vitezstvi";
		color = SDL_MapRGBA(surface->format, 0xFF, 0xFF, 0xFF, 128);
	}
	else if(this->loss = true)
	{
		verdict = "Prohra";
		color = SDL_MapRGBA(surface->format, 0xFF, 0x0, 0x0, 128);
	}

	SDL_FillRect(surface, NULL, color);
	applySurface(0, 0, surface, this->screen);

	SDL_Surface* text = TTF_RenderText_Solid(PM::Global::font, verdict.c_str(), PM::Global::fontColor);
	applySurface((PM::Global::screenWidth - text->w) / 2, 20, text, this->screen);

	text = TTF_RenderText_Solid(PM::Global::font, "r - restart", PM::Global::fontColor);
	applySurface((PM::Global::screenWidth - text->w) / 2, 20 + text->h + 5, text, this->screen);

	text = TTF_RenderText_Solid(PM::Global::font, "ESC - konec", PM::Global::fontColor);
	applySurface((PM::Global::screenWidth - text->w) / 2, 20 + 2 * text->h + 5, text, this->screen);

	SDL_FreeSurface(text);
	SDL_FreeSurface(surface);
}

void Game::mainLoop()
{
	bool quit = false;
	SDL_Event event;

	while(quit == false)
    {
		this->fps.start();

        while(SDL_PollEvent(&event))
        {
			if(this->victory != true && this->loss != true && this->pause == false)
			{
				for(int i = 0; i < characters.size(); i++)
				{
					characters[i]->handleEvent(event);
				}
			}

			if(event.type == SDL_KEYDOWN)
			{
				switch(event.key.keysym.sym)
				{
					case SDLK_ESCAPE: quit = true; break;
					case SDLK_r: this->restart(); break;
					case SDLK_p: this->pause = !this->pause; break;
				}
			}

            if(event.type == SDL_QUIT)
            {
                quit = true;
            }
        }

		if(this->victory != true && this->loss != true && this->pause == false)
		{
			for(int i = 0; i < bombs.size(); i++)
			{
				bombs[i]->control();
			}

			for(int i = 0; i < improvements.size(); i++)
			{
				for(int j = 0; j < characters.size(); j++)
				{
					if(improvements[i]->position == PM::Position(0, 0))
					{
						std::cout << "null position " << improvements.size() << std::endl;
						std::cin.get();
					}
					characters[j]->checkImprovement(improvements[i]);
				}
			}

			for(int i = 0; i < characters.size(); i++)
			{
				characters[i]->control();
				characters[i]->animate();
				characters[i]->move();
				map->placeCharacter(characters[i]);
			}
		
			map->control();
	
			this->map->show(this->screen);

			// for debug only
			for(int i = 0; i < characters.size(); i++)
			{
				characters[i]->show_path(this->screen);
			}
			//

			if(this->victory == true || this->loss == true)
			{
				this->showVerdict();
			}
		}

		if(SDL_Flip(this->screen) == -1)
		{
			throw PM::Exception("Cannon flip the screen surface");
		}
	
		if(this->fps.getTicks() < 1000 / PM::Global::FPS)
		{
			SDL_Delay((1000/PM::Global::FPS) - fps.getTicks());
		}

		if(this->pause == false) PM::Global::FPSCount++;
	}

}

}
