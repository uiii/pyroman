#ifndef PM_BOMB_H
#define PM_BOMB_H

#include <vector>

#include "global.h"
#include "timer.h"

namespace PM
{

class Tile; // forward declaration
class Character; // forward declaration

class Bomb
{
public:
	Bomb(PM::Character* owner, int range = 1);

	virtual void addTile(PM::Tile* tile);

	virtual PM::Position& getPosition() { return this->position; }
	virtual int getRange() { return this->range; }

	virtual void ownerDie();
	
	virtual void setTimeToExplode(int _value) { this->timeToExplode = _value; }

	virtual void control(); // control itsetf

	virtual void explode();
	virtual void vaporize();

	virtual void show(SDL_Surface* surface, PM::Position position);
	
	virtual int getRemainingTime();
	
	virtual bool isExploding() { return this->exploding; }
	
	sigc::signal<void, PM::Bomb*> signal_vaporize;

protected:
	PM::Position position;
	int range; // range of explosion
	int timeToExplode; // amount of time to explode in milliseconds
	int timeToVaporize; // amount of time to vaporization of the explosion in milliseconds

	bool exploding;

	PM::FPSTimer timer;

	PM::Character* owner;
	std::vector< PM::Tile* > tiles; // tiles that are in range and aren't indestrucable

	SDL_Rect texture;
};

}

#endif
