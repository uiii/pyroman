#include "global.h"

#include <sstream>

namespace PM
{
int PM::Global::FPSCount = 0;

int PM::Global::screenWidth = 1;
int PM::Global::screenHeight = 1;

SDL_Surface* PM::Global::textureSource = NULL;
TTF_Font* PM::Global::font = NULL;
SDL_Color PM::Global::fontColor = {0x0, 0x0, 0x0};

SDL_Surface* loadImage(const std::string& _filename)
{
	SDL_Surface* loadedImage = NULL;
	SDL_Surface* optimizedImage = NULL;

	loadedImage = IMG_Load(_filename.c_str());

	if(loadedImage != NULL)
	{
		optimizedImage = SDL_DisplayFormatAlpha(loadedImage);
		SDL_FreeSurface(loadedImage);
	}

	if(optimizedImage == NULL)
	{
		throw PM::Exception("Cannot load image: " + _filename);
	}

	return optimizedImage;
}

void initSDL()
{
	if(SDL_Init(SDL_INIT_EVERYTHING) == -1)
	{
		throw PM::Exception("SDL initialization failed");
	}

	putenv("SDL_VIDEO_CENTERED=TRUE");

	SDL_SetVideoMode(10, 10, PM::Global::SCREEN_BPP, SDL_SWSURFACE);

	if(TTF_Init() == -1)
	{
		throw PM::Exception("SDL_ttf initialization failed");
	}
	
	SDL_WM_SetCaption("Pyroman", NULL);
}

void cleanUpSDL()
{
	SDL_FreeSurface(PM::Global::textureSource);

	TTF_CloseFont(PM::Global::font);

	TTF_Quit();
	SDL_Quit();
}

void applySurface(int x, int y, SDL_Surface* source, SDL_Surface* destination)
{
	SDL_Rect offset;
	offset.x = x;
	offset.y = y;

	SDL_BlitSurface(source, NULL, destination, &offset);
}

void applySurface(SDL_Rect* sourceRect, SDL_Surface* source, SDL_Rect* targetRect, SDL_Surface* target)
{
	SDL_BlitSurface(source, sourceRect, target, targetRect);
}

SDL_Surface* createEmpySurface(int _width, int _height)
{
    SDL_Surface *surface;
    Uint32 rmask, gmask, bmask, amask;

#if SDL_BYTEORDER == SDL_BIG_ENDIAN
    rmask = 0xff000000;
    gmask = 0x00ff0000;
    bmask = 0x0000ff00;
    amask = 0x000000ff;
#else
    rmask = 0x000000ff;
    gmask = 0x0000ff00;
    bmask = 0x00ff0000;
    amask = 0xff000000;
#endif

	return SDL_CreateRGBSurface(SDL_SWSURFACE | SDL_SRCALPHA, _width, _height, PM::Global::SCREEN_BPP, rmask, gmask, bmask, amask);
}
	
std::ostream& operator<<(std::ostream& os, const PM::Position& _position)
{
	os << "(" << _position.x << ", " << _position.y << ")";

	return os;
}

std::string Convert::intToStr(int _n)
{
	std::stringstream ss;
	ss << _n;
	
	return ss.str();
}

int Convert::strToInt(const std::string& _str)
{
	std::stringstream ss(_str);
	int n;

	ss >> n;
	
	return n;
}

}
