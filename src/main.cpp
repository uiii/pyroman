#include <iostream>
#include <string>

#include "game.h"
#include "global.h"
#include "exception.h"

int main(int argc, char* argv[])
{
	try
	{
		PM::initSDL();

		std::string map;
		if(argc == 2)
		{
			map = argv[1];
		}
		else
		{
			map = "default.map";
		}

		PM::Game game(map, "textures.png");

		game.mainLoop();

		PM::cleanUpSDL();
	}
	catch(PM::Exception& ex)
	{
		std::cout << ex.what() << std::endl;
	}

	return 0;
}
