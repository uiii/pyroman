#include "timer.h"

namespace PM
{

Timer::Timer()
{
	this->startTicks["_noname_"] = 0;
	this->pausedTicks["_noname_"] = 0;
	this->paused["_noname_"] = false;
	this->started["_noname_"] = false;
}

void Timer::start(const std::string& _name)
{
	this->started[_name] = true;
	this->paused[_name] = false;
	this->startTicks[_name] = SDL_GetTicks();
}

void Timer::stop(const std::string& _name)
{
	if(this->find(_name) == true)
	{
		this->started[_name] = false;
		this->paused[_name] = false;
	}
}

void Timer::pause(const std::string& _name)
{
	if(this->find(_name) == true)
	{
		if((this->started[_name] == true) && (this->paused[_name] == false))
		{
			this->paused[_name] = true;
			this->pausedTicks[_name] = SDL_GetTicks() - this->startTicks[_name];
		}
	}
}

void Timer::unpause(const std::string& _name)
{
	if(this->find(_name) == true)
	{
		if(this->paused[_name] == true)
		{
			this->paused[_name] = false;
			this->startTicks[_name] = SDL_GetTicks() - pausedTicks[_name];
			this->pausedTicks[_name] = 0;
		}
	}
}

int Timer::getTicks(const std::string& _name)
{
	if(this->find(_name) == true)
	{
		if(this->started[_name] == true)
		{
			if(this->paused[_name] == true)
			{
				return this->pausedTicks[_name];
			}
			else
			{
				return SDL_GetTicks() - this->startTicks[_name];
			}
		}
	}

	return 0;
}

bool Timer::isStarted(const std::string& _name)
{
	if(this->find(_name) == true)
	{
		return this->started[_name];
	}

	return false;
}

bool Timer::isPaused(const std::string& _name)
{
	if(this->find(_name) == true)
	{
		return this->paused[_name];
	}

	return false;
}

bool Timer::find(const std::string& _name)
{
	if(started.find(_name) != started.end())
	{
		return true;
	}
	else
	{
		return false;
	}
}

// --- FPSTimer functions --- //

void FPSTimer::start(const std::string& _name)
{
	this->started[_name] = true;
	this->paused[_name] = false;
	this->startTicks[_name] = PM::Global::FPSCount; //SDL_GetTicks();
}

void FPSTimer::pause(const std::string& _name)
{
	if(this->find(_name) == true)
	{
		if((this->started[_name] == true) && (this->paused[_name] == false))
		{
			this->paused[_name] = true;
			this->pausedTicks[_name] = PM::Global::FPSCount - this->startTicks[_name]; //SDL_GetTicks() - this->startTicks[_name];
		}
	}
}

void FPSTimer::unpause(const std::string& _name)
{
	if(this->find(_name) == true)
	{
		if(this->paused[_name] == true)
		{
			this->paused[_name] = false;
			this->startTicks[_name] = PM::Global::FPSCount - this->pausedTicks[_name]; //SDL_GetTicks() - pausedTicks[_name];
			this->pausedTicks[_name] = 0;
		}
	}
}

int FPSTimer::getTicks(const std::string& _name)
{
	if(this->find(_name) == true)
	{
		if(this->started[_name] == true)
		{
			if(this->paused[_name] == true)
			{
				return this->pausedTicks[_name];
			}
			else
			{
				return (PM::Global::FPSCount - this->startTicks[_name]) * 1000/PM::Global::FPS; //SDL_GetTicks() - this->startTicks[_name];
			}
		}
	}

	return 0;
}

}
