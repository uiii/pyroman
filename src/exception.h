#ifndef PM_EXCEPTION_H
#define PM_EXCEPTION_H

#include <string>

namespace PM
{

class Exception
{
public:
	Exception(const std::string& _ex = ""):
		exception(_ex)
	{}

	const std::string& what()
	{
		return this->exception;
	}

private:
	std::string exception;
};

}

#endif
