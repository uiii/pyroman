#ifndef PM_CHARACTER_H
#define PM_CHARACTER_H

#include "global.h"

#include "timer.h"
#include "pathfinding/nodemap.h"

namespace PM
{

class Tile; // forward declaration
class Map; // forward declaration
class Bomb; // forward declaration

class Character
{
public:
	Character(int x, int y);

	virtual void handleEvent(SDL_Event& event) = 0;
	virtual void control() = 0; // control itself
	virtual void animate() = 0;
	virtual void show(SDL_Surface* surface);

	// for debug only
	virtual void show_path(SDL_Surface* surface) {}
	//

	virtual void move() = 0;
	virtual void die();

	virtual PM::Bomb* putBomb();
	virtual void bombVaporize(PM::Bomb* bomb);

	virtual void setPosition(PM::Position _position) { this->position = _position; }
	virtual PM::Position& getPosition() { return this->position; }

	virtual void setTile(PM::Tile* _tile) { this->tile = _tile; }
	virtual PM::Tile* getTile() { return this->tile; }

	virtual void checkImprovement(PM::Improvement* improvement) = 0;
	void improve(PM::Improvement* improvement);

	sigc::signal<void, PM::Bomb*> signal_addBomb;
	sigc::signal<void, PM::Character*> signal_die;
	sigc::signal<void> signal_pyromanDie;
	sigc::signal<void> signal_pyrobotDie;

protected:
	PM::Position position;
	int bombRange;
	int putBombsCount;
	int bombsAvailable;

	PM::Tile* tile;
	std::vector< PM::Bomb* > bombs;

	PM::FPSTimer timer;

	int currentTexture;
	SDL_Rect texture[2];
};

class Pyroman : public PM::Character
{
public:
	Pyroman(int _x, int _y):
		Character(_x, _y),
		xVel(0),
		yVel(0)
	{
		texture[0].x = 60;
		texture[0].y = 0;
		texture[0].w = 20;
		texture[0].h = 20;
	}

	void handleEvent(SDL_Event& event);
	void control() {}
	void animate() {}
	void move();

	void checkImprovement(PM::Improvement* improvement) {};

	void die();

private:
	int xVel, yVel;
};

enum PrimaryAction
{
	FINDING_IMPROVEMENTS = 1,
	FINDING_PLAYER = 2,
	BOMBING_PLAYER = 4
};

enum SecondaryAction
{
	NOTHING = 1,
	FIND_TILE_TO_BOMB_WALL = 2,
	FIND_SHELTER = 4,
	FIND_PRIMARY_PATH = 8
};

class Pyrobot : public PM::Character
{
public:
	Pyrobot(int _x, int _y, PM::Map& _map):
		Character(_x, _y),
		primaryNextTile(NULL),
		secondaryNextTile(NULL),
		primaryPath(NULL),
		secondaryPath(NULL),
		map(_map),
		tileMap(_map),
		primaryAction(FINDING_IMPROVEMENTS),
		secondaryAction(NOTHING),
		wantedImprovementPosition(0, 0),
		findAttemptsCount(1),
		stop(false)
	{
		texture[0].x = 80;
		texture[0].y = 0;
		texture[0].w = 20;
		texture[0].h = 20;

		timer.start("slowdown");
	}

	void handleEvent(SDL_Event& event) {}
	void control();
	void animate();
	void move();

	void checkImprovement(PM::Improvement* improvement);

	// for debugging only
	void show_path(SDL_Surface* _surface)
	{
		if(this->primaryPath != NULL) this->primaryPath->show(_surface);
		//std::cout << "mezi" << std::endl;
		if(this->secondaryPath != NULL) this->secondaryPath->show(_surface);
		//std::cout << "za" << std::endl;
	}

	void die();

private:
	PM::Tile* primaryNextTile;
	PM::Tile* secondaryNextTile;
	PM::PathFinding::NodePath* primaryPath;
	PM::PathFinding::NodePath* secondaryPath;
	PM::PathFinding::NodePath* escapePath;
	PM::PathFinding::NodeMap map;
	PM::Map& tileMap;

	PrimaryAction primaryAction;
	SecondaryAction secondaryAction;

	PM::Position wantedImprovementPosition;

	int findAttemptsCount;
	bool stop;
};

}

#endif
