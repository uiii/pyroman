#include "character.h"

#include <iostream>

#include "map.h"
#include "bomb.h"

namespace PM
{

// -- Character's functions -- //
Character::Character(int _x, int _y):
	position(_x, _y),
	bombRange(1),
	putBombsCount(0),
	bombsAvailable(1),
	tile(NULL),
	currentTexture(0)
{
	texture[1].x = 120;
	texture[1].y = 140;
	texture[1].w = 20;
	texture[1].h = 20;
}

void Character::die()
{
	this->tile->removeCharacter(this);

	for(int i = 0; i < this->bombs.size(); i++)
	{
		this->bombs[i]->ownerDie();
	}

	this->signal_die.emit(this);
}

PM::Bomb* Character::putBomb()
{
	if(this->putBombsCount < this->bombsAvailable)
	{
		PM::Bomb* bomb = new PM::Bomb(this, this->bombRange);

		bombs.push_back(bomb);

		this->signal_addBomb.emit(bomb);

		this->putBombsCount++;

		return bomb;
	}
	else
	{
		return NULL;
	}
}

void Character::bombVaporize(PM::Bomb* _bomb)
{
	this->putBombsCount--;

	std::vector< PM::Bomb* >::iterator it;
	for(it = this->bombs.begin(); it < this->bombs.end(); it++)
	{
		if(*it == _bomb)
		{
			this->bombs.erase(it);
			break;
		}
	}

}

void Character::show(SDL_Surface* _surface)
{
	SDL_Rect target;
	target.x = this->position.x * 20;
	target.y = this->position.y * 20;

	applySurface(&this->texture[this->currentTexture], PM::Global::textureSource, &target, _surface);
}

void Character::improve(PM::Improvement* _improvement)
{
	switch(_improvement->type)
	{
		case BOMB:
			this->bombsAvailable++;
			break;

		case RANGE:
			this->bombRange++;
			break;
	}
}

// -- Pyroman's functions -- //
void Pyroman::handleEvent(SDL_Event& event)
{
	if(event.type == SDL_KEYDOWN)
	{
		switch(event.key.keysym.sym)
		{
			case SDLK_UP: this->yVel = -1; break;
			case SDLK_DOWN: this->yVel = +1; break;
			case SDLK_LEFT: this->xVel = -1; break;
			case SDLK_RIGHT: this->xVel = +1; break;
			case SDLK_SPACE: this->putBomb(); break;
		}
	}

	if(event.type == SDL_KEYUP)
	{
		switch(event.key.keysym.sym)
		{
			case SDLK_UP: this->yVel = 0; break;
			case SDLK_DOWN: this->yVel = 0; break;
			case SDLK_LEFT: this->xVel = 0; break;
			case SDLK_RIGHT: this->xVel = 0; break;
		}
	}
}

void Pyroman::move()
{
	this->position.x += this->xVel;
	this->position.y += this->yVel;

	this->yVel = 0;
	this->xVel = 0;
}

void Pyroman::die()
{
	PM::Character::die();
	this->signal_pyromanDie.emit();
	delete this;
}

// -- Pyrobot's functions -- //
void Pyrobot::control()
{
	if(this->timer.getTicks("slowdown") < PM::Global::AISlowDown && this->timer.isStarted("slowdown") == true) return;
	//if(this->timer.getTicks("wait") < PM::Global::bombTimeToExplode + PM::Global::bombTimeToVaporize && this->timer.isStarted("wait") == true) return;

	if(this->stop == true)
	{
		this->stop = false;
	}

	// TODO misto modu pouzit akce
	// zde se rozhodne jakou akci bude provadet

	// check if the player is within bomb range
	PM::PathFinding::FindProperties properties;
	properties.playerXDistance = this->bombRange;
	properties.playerYDistance = this->bombRange;

	if(this->map.checkCurrentPosition(this->position, PM::PathFinding::PLAYER_DISTANCE, properties) && this->wantedImprovementPosition == PM::Position(0, 0))
	{
		std::cout << "bombing" << std::endl;
		if(this->primaryAction != BOMBING_PLAYER)
		{
			delete this->primaryPath;
			delete this->secondaryPath;
			this->primaryPath = NULL;
			this->secondaryPath = NULL;
			
			this->primaryNextTile = NULL;
			this->secondaryNextTile = NULL;

			this->secondaryAction = NOTHING;
		}

		this->primaryAction = BOMBING_PLAYER;
	}
	else if((this->tileMap.getWallsCount() < this->tileMap.getInitialWallsCount() * 0.05 ||
			this->tileMap.getImprovementsCount() < this->tileMap.getInitialImprovementsCount() * 0.1 ||
			(this->bombRange > 2 && this->bombsAvailable > 1)) && this->wantedImprovementPosition == PM::Position(0, 0))
	{
		std::cout << "zdi " << this->tileMap.getWallsCount() << std::endl;
		std::cout << "vylepseni " << this->tileMap.getImprovementsCount() << std::endl;
		std::cout << "bomb range " << this->bombRange << std::endl;
		std::cout << "bomb avail " << this->bombsAvailable << std::endl;
		
		//std::cin.get();
		if(this->primaryAction != FINDING_PLAYER)
		{
			delete this->primaryPath;
			delete this->secondaryPath;
			this->primaryPath = NULL;
			this->secondaryPath = NULL;
			
			this->primaryNextTile = NULL;
			this->secondaryNextTile = NULL;

			this->secondaryAction = NOTHING;
		}

		this->primaryAction = FINDING_PLAYER;
	}
	else
	{
		std::cout << "impr" << std::endl;
		if(this->primaryAction != FINDING_IMPROVEMENTS)
		{
			if(this->wantedImprovementPosition == PM::Position(0, 0))
			{
				delete this->primaryPath;
				this->primaryPath = NULL;
			}

			delete this->secondaryPath;
			this->secondaryPath = NULL;
			
			this->primaryNextTile = NULL;
			this->secondaryNextTile = NULL;

			this->secondaryAction = NOTHING;
		}

		this->primaryAction = FINDING_IMPROVEMENTS;
	}

	switch(this->primaryAction)
	{
		case FINDING_IMPROVEMENTS:
		{
			PM::Bomb* bomb = NULL;

			// re-connection from SECONDARY PATH to PRIMARY PATH
			if(this->secondaryAction != NOTHING && this->primaryNextTile != NULL)
			{
				if(this->position == this->primaryNextTile->getPosition())
				{
					delete this->secondaryPath;
					this->secondaryAction = NOTHING;
					this->secondaryNextTile = NULL;
					this->secondaryPath = NULL;
					this->primaryNextTile = NULL;
				}
			}
			
			// go through PRIMARY PATH
			if(this->secondaryPath == NULL && this->secondaryAction == NOTHING)
			{
				if(this->primaryPath == NULL)
				{
					this->primaryNextTile = NULL;
					this->primaryPath = this->map.findWall(this->position, this->bombRange);
				}

				if(this->primaryPath != NULL)
				{
					if(this->primaryPath->empty())
					{
						if(this->primaryNextTile == NULL) // pyrobot is staying on the end of path
						{
							// clean up memory
							delete this->primaryPath;
							this->primaryPath = NULL;

							if(this->wantedImprovementPosition != PM::Position(0, 0))
							{
								this->wantedImprovementPosition = PM::Position(0, 0);
								this->primaryPath = this->map.findShelter(this->position);
							}
							else
							{
								// check if the player is within bomb range
								PM::PathFinding::FindProperties properties;
								properties.viewType = WALL;
								properties.viewDistance = this->bombRange;

								if(this->map.checkCurrentPosition(this->position, PM::PathFinding::TILE_VIEW_TYPE | PM::PathFinding::TILE_VIEW_IS_CLEAR, properties))
								{
									// polozit bombu
									bomb = this->putBomb();
									////std::cin.get();
								}
								
								// vyhledat pozici pro bombardovani
								this->primaryPath = this->map.findWall(this->position, this->bombRange, false);
								//std::cout << this->primaryPath->getEnd()->getPosition();
								////std::cin.get();

								if(this->primaryPath == NULL)
								{
									this->primaryPath = this->map.findShelter(this->position);
									if(this->primaryPath == NULL && bomb != NULL)
									{
										bomb->vaporize();
										this->primaryPath = this->map.findWall(this->position, this->bombRange, false);
									}
								}
							}
						}
					}
					else // the path continues
					{
						if(this->wantedImprovementPosition != PM::Position(0, 0))
						{
							if(this->tileMap.getTile(this->wantedImprovementPosition)->hasImprovement() == false)
							{
								this->primaryNextTile = NULL;
								this->wantedImprovementPosition = PM::Position(0, 0);
								this->primaryPath = this->map.findShelter(this->position);
							}
						}
						else if(this->primaryPath->checkEnd() == false) // if the end tile hasn't the same properties as we found it
						{
							this->primaryNextTile = NULL;
							this->primaryPath = this->map.findTheSame(this->position, this->primaryPath);
						}
					}
				}

				// if no path have been found, try to find at least some shelter
				if(this->primaryPath == NULL)
				{
					this->primaryPath = this->map.findShelter(this->position);
				}
				
				// move to next tile in path
				if(this->primaryPath != NULL)
				{
					if(this->primaryNextTile == NULL && this->primaryPath->empty() == false)
					{
						this->primaryNextTile = this->primaryPath->next();
					}
				}

				// check next tile
				if(this->primaryNextTile != NULL)
				{
					int waitMoves = 0;
					if(this->primaryNextTile->isClear() == false)
					{
						if(this->primaryPath->isCurrentTileChanged() == true)
						{
							//std::cin.get();
							if(this->map.findShelter(this->primaryNextTile->getPosition(), PM::Global::AISlowDown) == NULL)
							{
								// cekat nebo zdrhat
								// zjistit kolik tahu musim pockat
								// pokud to nelze preckat, zdrhnout do ukrytu
								
								waitMoves = ceil((float) this->primaryNextTile->getState() / PM::Global::AISlowDown);
								this->stop = true;

								std::cout << this->primaryNextTile->getState() << " " << PM::Global::AISlowDown << std::endl;
								std::cout << waitMoves << std::endl;
								//std::cin.get();
								
							}
							else
							{
								if(this->primaryNextTile->getState() < PM::Global::AISlowDown)
								{
									waitMoves = 1;
									this->stop = true;
								}
							}
						}
						else if(this->primaryNextTile->getState() < PM::Global::AISlowDown)
						{
							waitMoves = 1;
							this->stop = true;
						}
					}

					if(this->stop == true)
					{
						if(this->tileMap.getTile(this->position)->getState() < waitMoves * PM::Global::AISlowDown && this->tileMap.getTile(this->position)->isClear() == false)
						{
							// utect
							this->secondaryAction = FIND_SHELTER;
							this->secondaryNextTile = NULL;
							this->secondaryPath = this->map.findShelter(this->position);

							this->stop = false;

							std::cout << this->secondaryPath << std::endl;
							//std::cin.get();
						}
					}
				}
			}
			
			// go through SECONDARY PATH
			if(this->secondaryPath != NULL)
			{
				if(this->secondaryPath->empty())
				{
					if(this->secondaryNextTile == NULL)
					{
						// clean up memory
						delete this->secondaryPath;
						this->secondaryPath = NULL;

						if(secondaryAction == FIND_SHELTER)
						{
							// je v ukrytu
							if(this->primaryNextTile->getType() == WALL)
							{	
								if(this->primaryNextTile->isClear() == true)
								{
									this->secondaryAction = FIND_TILE_TO_BOMB_WALL;
									this->secondaryPath = this->map.findTileToBombPosition(this->position, this->primaryNextTile->getPosition(), this->bombRange);
								}
								else
								{
									this->secondaryAction = FIND_SHELTER;
									this->secondaryPath = this->map.findShelter(this->position);
								}
							}
							else
							{
								this->secondaryAction = FIND_PRIMARY_PATH;
								this->secondaryPath = this->map.findPosition(this->position, this->primaryNextTile->getPosition());
							}

							if(this->secondaryPath == NULL)
							{
								////std::cin.get();
							}
						}
						else if(secondaryAction == FIND_TILE_TO_BOMB_WALL)
						{
							// polozit bombu a schovat se
							bomb = this->putBomb();
							
							this->secondaryAction = FIND_SHELTER;
							this->secondaryPath = this->map.findShelter(this->position);
							if(this->secondaryPath == NULL && bomb != NULL)
							{
								bomb->vaporize(); // unput bomb
								this->secondaryAction = FIND_TILE_TO_BOMB_WALL;
								this->secondaryPath = this->map.findTileToBombPosition(this->position, this->primaryNextTile->getPosition(), this->bombRange, false);
								if(this->secondaryPath == NULL)
								{
									// if the wall can't be destroyed, then find another primary path
									this->secondaryAction = NOTHING;
									this->primaryPath = this->map.findTheSame(this->position, this->primaryPath, this->primaryNextTile);
									break;
								}
							}
						}
					}
				}
				else
				{
					if(this->secondaryPath->checkEnd() == false) // if the end tile isn't in the same state as we found it
					{
						this->secondaryNextTile = NULL;
						this->secondaryPath = this->map.findTheSame(this->position, this->secondaryPath);
					}
				}

				// if no path have been found, find at leat some shelter
				if(this->secondaryPath == NULL)
				{
					this->secondaryAction = FIND_SHELTER;
					this->secondaryPath = this->map.findShelter(this->position);
				}

				// move to next tile
				if(this->secondaryPath != NULL)
				{
					if(this->secondaryNextTile == NULL && this->secondaryPath->empty() == false)
					{
						this->secondaryNextTile = this->secondaryPath->next();
					}
				}

				// check next tile
				if(this->secondaryNextTile != NULL)
				{
					int waitMoves = 0;
					if(this->secondaryNextTile->isClear() == false)
					{
						if(this->secondaryPath->isCurrentTileChanged() == true)
						{
							//std::cin.get();
							if(this->map.findShelter(this->secondaryNextTile->getPosition(), PM::Global::AISlowDown) == NULL)
							{
								// cekat nebo zdrhat
								// zjistit kolik tahu musim pockat
								// pokud to nelze preckat, zdrhnout do ukrytu
								
								waitMoves = ceil((float) this->secondaryNextTile->getState() / PM::Global::AISlowDown);
								this->stop = true;
								
								std::cout << this->secondaryNextTile->getState() << " " << PM::Global::AISlowDown << std::endl;
								std::cout << waitMoves << std::endl;
								//std::cin.get();
							}
							else if(this->secondaryNextTile->getState() < PM::Global::AISlowDown)
							{
								waitMoves = 1;
								this->stop = true;
							}
						}
						else if(this->secondaryNextTile->getState() < PM::Global::AISlowDown)
						{
							waitMoves = 1;
							this->stop = true;
						}
					}

					if(this->stop == true)
					{
						if(this->tileMap.getTile(this->position)->getState() <= waitMoves * PM::Global::AISlowDown && this->tileMap.getTile(this->position)->isClear() == false)
						{
							this->stop = false;
							// utect
							this->secondaryAction = FIND_SHELTER;
							this->secondaryNextTile = NULL;
							this->secondaryPath = this->map.findShelter(this->position);

							if(this->secondaryPath != NULL)
							{
								this->secondaryNextTile = this->secondaryPath->next();
							}
						}
					}
				}
			}
		}

		break;
		
		case FINDING_PLAYER:
		{
			PM::Bomb* bomb = NULL;

			// re-connection from SECONDARY PATH to PRIMARY PATH
			if(this->secondaryAction != NOTHING && this->primaryNextTile != NULL)
			{
				if(this->position == this->primaryNextTile->getPosition())
				{
					delete this->secondaryPath;
					this->secondaryAction = NOTHING;
					this->secondaryNextTile = NULL;
					this->secondaryPath = NULL;
					this->primaryNextTile = NULL;
				}
			}
			
			// go through PRIMARY PATH
			if(this->secondaryPath == NULL && this->secondaryAction == NOTHING)
			{
				if(this->primaryPath == NULL)
				{
					this->primaryNextTile = NULL;
					this->primaryPath = this->map.findTileCloserToPlayer(this->position, this->bombRange);
				}

				if(this->primaryPath != NULL)
				{
					if(this->primaryPath->empty())
					{
						if(this->primaryNextTile == NULL) // pyrobot is staying on the end of path
						{
							// clean up memory
							delete this->primaryPath;
							this->primaryPath = NULL;

							this->primaryPath = this->map.findTileCloserToPlayer(this->position, this->bombRange, false);
						}
					}
					else // the path continues
					{
						if(this->primaryPath->checkEnd() == false) // if the end tile hasn't the same properties as we found it
						{
							this->primaryNextTile = NULL;
							this->primaryPath = this->map.findTheSame(this->position, this->primaryPath);
						}
					}
				}

				// if no path have been found, try to find at least some shelter
				if(this->primaryPath == NULL)
				{
					this->primaryPath = this->map.findShelter(this->position);
				}
				
				// move to next tile in path
				if(this->primaryPath != NULL)
				{
					if(this->primaryNextTile == NULL && this->primaryPath->empty() == false)
					{
						this->primaryNextTile = this->primaryPath->next();
					}
				}

				// check next tile
				if(this->primaryNextTile != NULL)
				{
					int waitMoves = 0;
					if(this->primaryNextTile->getType() == WALL)
					{
						if(this->primaryNextTile->isClear() == true)
						{
							// najit cestu na policko, odkud lze bombardovat prekazejici zed
							this->secondaryAction = FIND_TILE_TO_BOMB_WALL;
							this->secondaryNextTile = NULL;
							this->secondaryPath = this->map.findTileToBombPosition(this->position, this->primaryNextTile->getPosition(), this->bombRange);
						}
						else
						{
							// najit ukryt, pokud stoji na bombe, nebo cekat
							waitMoves = ceil((float) this->primaryNextTile->getState() / PM::Global::AISlowDown);
							this->stop = true;
						}
					}
					else if(this->primaryNextTile->isClear() == false)
					{
						if(this->primaryPath->isCurrentTileChanged() == true)
						{
							//std::cin.get();
							if(this->map.findShelter(this->primaryNextTile->getPosition(), PM::Global::AISlowDown) == NULL)
							{
								// cekat nebo zdrhat
								// zjistit kolik tahu musim pockat
								// pokud to nelze preckat, zdrhnout do ukrytu
								
								waitMoves = ceil((float) this->primaryNextTile->getState() / PM::Global::AISlowDown);
								this->stop = true;

								std::cout << this->primaryNextTile->getState() << " " << PM::Global::AISlowDown << std::endl;
								std::cout << waitMoves << std::endl;
								//std::cin.get();
								
							}
							else
							{
								if(this->primaryNextTile->getState() < PM::Global::AISlowDown)
								{
									waitMoves = 1;
									this->stop = true;
								}
							}
						}
						else if(this->primaryNextTile->getState() < PM::Global::AISlowDown)
						{
							waitMoves = 1;
							this->stop = true;
						}
					}

					if(this->stop == true)
					{
						if(this->tileMap.getTile(this->position)->getState() < waitMoves * PM::Global::AISlowDown && this->tileMap.getTile(this->position)->isClear() == false)
						{
							// utect
							this->secondaryAction = FIND_SHELTER;
							this->secondaryNextTile = NULL;
							this->secondaryPath = this->map.findShelter(this->position);

							this->stop = false;

							std::cout << this->secondaryPath << std::endl;
							//std::cin.get();
						}
					}
				}
			}
			
			// go through SECONDARY PATH
			if(this->secondaryPath != NULL)
			{
				if(this->secondaryPath->empty())
				{
					if(this->secondaryNextTile == NULL)
					{
						// clean up memory
						delete this->secondaryPath;
						this->secondaryPath = NULL;

						if(secondaryAction == FIND_SHELTER)
						{
							// je v ukrytu
							if(this->primaryNextTile->getType() == WALL)
							{
								if(this->primaryNextTile->isClear() == true)
								{
									this->secondaryAction = FIND_TILE_TO_BOMB_WALL;
									this->secondaryPath = this->map.findTileToBombPosition(this->position, this->primaryNextTile->getPosition(), this->bombRange);
								}
								else
								{
									this->secondaryAction = FIND_SHELTER;
									this->secondaryPath = this->map.findShelter(this->position);
								}
							}
							else
							{
								this->secondaryAction = FIND_PRIMARY_PATH;
								this->secondaryPath = this->map.findPosition(this->position, this->primaryNextTile->getPosition());
							}

							if(this->secondaryPath == NULL)
							{
								////std::cin.get();
							}
						}
						else if(secondaryAction == FIND_TILE_TO_BOMB_WALL)
						{
							// polozit bombu a schovat se
							bomb = this->putBomb();
							
							this->secondaryAction = FIND_SHELTER;
							this->secondaryPath = this->map.findShelter(this->position);
							if(this->secondaryPath == NULL && bomb != NULL)
							{
								bomb->vaporize(); // unput bomb
								this->secondaryAction = FIND_TILE_TO_BOMB_WALL;
								this->secondaryPath = this->map.findTileToBombPosition(this->position, this->primaryNextTile->getPosition(), this->bombRange, false);
								if(this->secondaryPath == NULL)
								{
									// if the wall can't be destroyed, then find another primary path
									this->secondaryAction = NOTHING;
									this->primaryPath = this->map.findTheSame(this->position, this->primaryPath, this->primaryNextTile);
									break;
								}
							}
						}
					}
				}
				else
				{
					if(this->secondaryPath->checkEnd() == false) // if the end tile isn't in the same state as we found it
					{
						this->secondaryNextTile = NULL;
						this->secondaryPath = this->map.findTheSame(this->position, this->secondaryPath);
					}
				}

				// if no path have been found, find at leat some shelter
				if(this->secondaryPath == NULL)
				{
					this->secondaryAction = FIND_SHELTER;
					this->secondaryPath = this->map.findShelter(this->position);
				}

				// move to next tile
				if(this->secondaryPath != NULL)
				{
					if(this->secondaryNextTile == NULL && this->secondaryPath->empty() == false)
					{
						this->secondaryNextTile = this->secondaryPath->next();
					}
				}

				// check next tile
				if(this->secondaryNextTile != NULL)
				{
					int waitMoves = 0;
					if(this->secondaryNextTile->isClear() == false)
					{
						if(this->secondaryPath->isCurrentTileChanged() == true)
						{
							//std::cin.get();
							if(this->map.findShelter(this->secondaryNextTile->getPosition(), PM::Global::AISlowDown) == NULL)
							{
								// cekat nebo zdrhat
								// zjistit kolik tahu musim pockat
								// pokud to nelze preckat, zdrhnout do ukrytu
								
								waitMoves = ceil((float) this->secondaryNextTile->getState() / PM::Global::AISlowDown);
								this->stop = true;
								
								std::cout << this->secondaryNextTile->getState() << " " << PM::Global::AISlowDown << std::endl;
								std::cout << waitMoves << std::endl;
								//std::cin.get();
							}
							else if(this->secondaryNextTile->getState() < PM::Global::AISlowDown)
							{
								waitMoves = 1;
								this->stop = true;
							}
						}
						else if(this->secondaryNextTile->getState() < PM::Global::AISlowDown)
						{
							waitMoves = 1;
							this->stop = true;
						}
					}

					if(this->stop == true)
					{
						if(this->tileMap.getTile(this->position)->getState() <= waitMoves * PM::Global::AISlowDown && this->tileMap.getTile(this->position)->isClear() == false)
						{
							this->stop = false;
							// utect
							this->secondaryAction = FIND_SHELTER;
							this->secondaryNextTile = NULL;
							this->secondaryPath = this->map.findShelter(this->position);

							if(this->secondaryPath != NULL)
							{
								this->secondaryNextTile = this->secondaryPath->next();
							}
						}
					}
				}
			}
		}

		break;

		case BOMBING_PLAYER:
		{
			PM::Bomb* bomb = NULL;

			// re-connection from SECONDARY PATH to PRIMARY PATH
			if(this->secondaryAction != NOTHING && this->primaryNextTile != NULL)
			{
				if(this->position == this->primaryNextTile->getPosition())
				{
					delete this->secondaryPath;
					this->secondaryAction = NOTHING;
					this->secondaryNextTile = NULL;
					this->secondaryPath = NULL;
					this->primaryNextTile = NULL;
				}
			}
			
			// go through PRIMARY PATH
			if(this->secondaryPath == NULL && this->secondaryAction == NOTHING)
			{
				if(this->primaryPath == NULL)
				{
					this->primaryNextTile = NULL;
					this->primaryPath = this->map.findTileToBombPlayer(this->position, this->bombRange);
				}

				if(this->primaryPath != NULL)
				{
					if(this->primaryPath->empty())
					{
						if(this->primaryNextTile == NULL) // pyrobot is staying on the end of path
						{
							// clean up memory
							delete this->primaryPath;
							this->primaryPath = NULL;

							// check if the player is within bomb range
							PM::PathFinding::FindProperties properties;
							properties.playerXDistance = this->bombRange;
							properties.playerYDistance = this->bombRange;

							if(this->map.checkCurrentPosition(this->position, PM::PathFinding::TILE_TO_BOMB_PLAYER | PM::PathFinding::PLAYER_DISTANCE, properties))
							{
								// polozit bombu
								bomb = this->putBomb();
								////std::cin.get();
							}
							
							// vyhledat pozici pro bombardovani
							this->primaryPath = this->map.findTileToBombPlayer(this->position, this->bombRange, false);
							//std::cout << this->primaryPath->getEnd()->getPosition();
							////std::cin.get();

							if(this->primaryPath == NULL)
							{
								this->primaryPath = this->map.findShelter(this->position);
								if(this->primaryPath == NULL && bomb != NULL)
								{
									bomb->vaporize();
									this->primaryPath = this->map.findTileToBombPlayer(this->position, this->bombRange, false);
								}
							}
						}
					}
					else // the path continues
					{
						if(this->primaryPath->checkEnd() == false) // if the end tile hasn't the same properties as we found it
						{
							this->primaryNextTile = NULL;
							this->primaryPath = this->map.findTheSame(this->position, this->primaryPath);
						}
					}
				}

				// if no path have been found, try to find at least some shelter
				if(this->primaryPath == NULL)
				{
					this->primaryPath = this->map.findShelter(this->position);
				}
				
				// move to next tile in path
				if(this->primaryPath != NULL)
				{
					if(this->primaryNextTile == NULL && this->primaryPath->empty() == false)
					{
						this->primaryNextTile = this->primaryPath->next();
					}
				}

				// check next tile
				if(this->primaryNextTile != NULL)
				{
					int waitMoves = 0;
					if(this->primaryNextTile->getType() == WALL)
					{
						if(this->primaryNextTile->isClear() == true)
						{
							// najit cestu na policko, odkud lze bombardovat prekazejici zed
							this->secondaryAction = FIND_TILE_TO_BOMB_WALL;
							this->secondaryNextTile = NULL;
							this->secondaryPath = this->map.findTileToBombPosition(this->position, this->primaryNextTile->getPosition(), this->bombRange);
						}
						else
						{
							// najit ukryt, pokud stoji na bombe, nebo cekat
							waitMoves = ceil((float) this->primaryNextTile->getState() / PM::Global::AISlowDown);
							this->stop = true;
						}
					}
					else if(this->primaryNextTile->isClear() == false)
					{
						if(this->primaryPath->isCurrentTileChanged() == true)
						{
							//std::cin.get();
							if(this->map.findShelter(this->primaryNextTile->getPosition(), PM::Global::AISlowDown) == NULL)
							{
								// cekat nebo zdrhat
								// zjistit kolik tahu musim pockat
								// pokud to nelze preckat, zdrhnout do ukrytu
								
								waitMoves = ceil((float) this->primaryNextTile->getState() / PM::Global::AISlowDown);
								this->stop = true;

								std::cout << this->primaryNextTile->getState() << " " << PM::Global::AISlowDown << std::endl;
								std::cout << waitMoves << std::endl;
								//std::cin.get();
								
							}
							else
							{
								if(this->primaryNextTile->getState() < PM::Global::AISlowDown)
								{
									waitMoves = 1;
									this->stop = true;
								}
							}
						}
						else if(this->primaryNextTile->getState() < PM::Global::AISlowDown)
						{
							waitMoves = 1;
							this->stop = true;
						}
					}

					if(this->stop == true)
					{
						if(this->tileMap.getTile(this->position)->getState() < waitMoves * PM::Global::AISlowDown && this->tileMap.getTile(this->position)->isClear() == false)
						{
							// utect
							this->secondaryAction = FIND_SHELTER;
							this->secondaryNextTile = NULL;
							this->secondaryPath = this->map.findShelter(this->position);

							this->stop = false;

							std::cout << this->secondaryPath << std::endl;
							//std::cin.get();
						}
					}
				}
			}
			
			// go through SECONDARY PATH
			if(this->secondaryPath != NULL)
			{
				if(this->secondaryPath->empty())
				{
					if(this->secondaryNextTile == NULL)
					{
						// clean up memory
						delete this->secondaryPath;
						this->secondaryPath = NULL;

						if(secondaryAction == FIND_SHELTER)
						{
							// je v ukrytu
							if(this->primaryNextTile->getType() == WALL)
							{	
								if(this->primaryNextTile->isClear() == true)
								{
									this->secondaryAction = FIND_TILE_TO_BOMB_WALL;
									this->secondaryPath = this->map.findTileToBombPosition(this->position, this->primaryNextTile->getPosition(), this->bombRange);
								}
								else
								{
									this->secondaryAction = FIND_SHELTER;
									this->secondaryPath = this->map.findShelter(this->position);
								}
							}
							else
							{
								this->secondaryAction = FIND_PRIMARY_PATH;
								this->secondaryPath = this->map.findPosition(this->position, this->primaryNextTile->getPosition());
							}

							if(this->secondaryPath == NULL)
							{
								////std::cin.get();
							}
						}
						else if(secondaryAction == FIND_TILE_TO_BOMB_WALL)
						{
							// polozit bombu a schovat se
							bomb = this->putBomb();
							
							this->secondaryAction = FIND_SHELTER;
							this->secondaryPath = this->map.findShelter(this->position);
							if(this->secondaryPath == NULL && bomb != NULL)
							{
								bomb->vaporize(); // unput bomb
								this->secondaryAction = FIND_TILE_TO_BOMB_WALL;
								this->secondaryPath = this->map.findTileToBombPosition(this->position, this->primaryNextTile->getPosition(), this->bombRange, false);
								if(this->secondaryPath == NULL)
								{
									// if the wall can't be destroyed, then find another primary path
									this->secondaryAction = NOTHING;
									this->primaryPath = this->map.findTheSame(this->position, this->primaryPath, this->primaryNextTile);
									break;
								}
							}
						}
					}
				}
				else
				{
					if(this->secondaryPath->checkEnd() == false) // if the end tile isn't in the same state as we found it
					{
						this->secondaryNextTile = NULL;
						this->secondaryPath = this->map.findTheSame(this->position, this->secondaryPath);
					}
				}

				// if no path have been found, find at leat some shelter
				if(this->secondaryPath == NULL)
				{
					this->secondaryAction = FIND_SHELTER;
					this->secondaryPath = this->map.findShelter(this->position);
				}

				// move to next tile
				if(this->secondaryPath != NULL)
				{
					if(this->secondaryNextTile == NULL && this->secondaryPath->empty() == false)
					{
						this->secondaryNextTile = this->secondaryPath->next();
					}
				}

				// check next tile
				if(this->secondaryNextTile != NULL)
				{
					int waitMoves = 0;
					if(this->secondaryNextTile->isClear() == false)
					{
						if(this->secondaryPath->isCurrentTileChanged() == true)
						{
							//std::cin.get();
							if(this->map.findShelter(this->secondaryNextTile->getPosition(), PM::Global::AISlowDown) == NULL)
							{
								// cekat nebo zdrhat
								// zjistit kolik tahu musim pockat
								// pokud to nelze preckat, zdrhnout do ukrytu
								
								waitMoves = ceil((float) this->secondaryNextTile->getState() / PM::Global::AISlowDown);
								this->stop = true;
								
								std::cout << this->secondaryNextTile->getState() << " " << PM::Global::AISlowDown << std::endl;
								std::cout << waitMoves << std::endl;
								//std::cin.get();
							}
							else if(this->secondaryNextTile->getState() < PM::Global::AISlowDown)
							{
								waitMoves = 1;
								this->stop = true;
							}
						}
						else if(this->secondaryNextTile->getState() < PM::Global::AISlowDown)
						{
							waitMoves = 1;
							this->stop = true;
						}
					}

					if(this->stop == true)
					{
						if(this->tileMap.getTile(this->position)->getState() <= waitMoves * PM::Global::AISlowDown && this->tileMap.getTile(this->position)->isClear() == false)
						{
							this->stop = false;
							// utect
							this->secondaryAction = FIND_SHELTER;
							this->secondaryNextTile = NULL;
							this->secondaryPath = this->map.findShelter(this->position);

							if(this->secondaryPath != NULL)
							{
								this->secondaryNextTile = this->secondaryPath->next();
							}
						}
					}
				}
			}
		}

		break;
	}
				
	std::cout << "pNT " << this->primaryNextTile << std::endl;
	std::cout << "sNT " << this->secondaryNextTile << std::endl;

	timer.start("slowdown");
}

void Pyrobot::animate()
{
	if(timer.isStarted("animation") == true)
	{
		if(timer.getTicks("animation") >= 100)
		{
			this->texture[0].y += 20;
			if(this->texture[0].y == 160) this->texture[0].y = 0;

			timer.start("animation");
		}
	}
	else
	{
		timer.start("animation");
	}
}

void Pyrobot::move()
{
	if(this->stop == false)
	{
		std::cout << "pos: (" << this->position.x << ", " << this->position.y << ")" << std::endl;
		std::cout << "sP: " << secondaryPath << std::endl;
		std::cout << "pP: " << primaryPath << std::endl;
		std::cout << "sNT: " << secondaryNextTile << std::endl;
		std::cout << "pNT: " << primaryNextTile << std::endl;
		if(this->secondaryPath != NULL)
		{
			if(this->secondaryNextTile != NULL)
			{
				std::cout << "move to " << this->secondaryNextTile->getState() << std::endl;
				this->position = this->secondaryNextTile->getPosition();
				this->secondaryNextTile = NULL;
				timer.start("slowdown");
			}
		}
		else if(this->primaryNextTile != NULL)
		{
			std::cout << "move to " << this->primaryNextTile->getState() << std::endl;
			this->position = this->primaryNextTile->getPosition();
			this->primaryNextTile = NULL;
			timer.start("slowdown");
		}
	}
	else
	{
		std::cout << "STOOOOOOOOOOOOP" << std::endl;
	}
}

void Pyrobot::checkImprovement(PM::Improvement* _improvement)
{
	// TODO !!!!!!!!!!!!!!!!!!
	int distance = abs(_improvement->position.x - this->position.x) + abs(_improvement->position.y - this->position.y);
	if(wantedImprovementPosition == PM::Position(0, 0))
	{
		if(distance < 4)
		{
			wantedImprovementPosition = _improvement->position;
			PM::PathFinding::NodePath* path = this->map.findPathToImprovement(this->position, _improvement->position, 10);
			if(path != NULL)
			{
				this->primaryNextTile = NULL;
				this->primaryPath = path;
			}
			else
			{
				this->wantedImprovementPosition = PM::Position(0, 0);
			}
		}
	}
}

void Pyrobot::die()
{
	PM::Character::die();
	this->signal_pyrobotDie.emit();
	delete this;
}

}
