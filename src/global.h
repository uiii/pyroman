#ifndef PM_GLOBAL_H
#define PM_GLOBAL_H

#include "SDL/SDL.h"
#include "SDL/SDL_image.h"
#include "SDL/SDL_ttf.h"
#include "SDL/SDL_getenv.h"

#include "sigc++/sigc++.h"

#include <iostream>
#include <string>
#include <cmath>

#include "exception.h"

namespace PM
{
	class Position
	{
	public:
		Position(int _x, int _y):
			x(_x),
			y(_y)
		{}

		PM::Position move(int i)
		{
			PM::Position newPosition(this->x, this->y);
			switch(i)
			{
				case 1: newPosition.y -= 1; break; // move upward
				case 2: newPosition.y += 1; break; // move downward
				case 3: newPosition.x -= 1; break; // move left
				case 4: newPosition.x += 1; break; // move right
				default: break; // else nothing
			}

			return newPosition;
		}

		bool operator==(const PM::Position& _position) const
		{
			return (this->x == _position.x && this->y == _position.y);
		}

		bool operator!=(const PM::Position& _position) const
		{
			return (this->x != _position.x || this->y != _position.y);
		}

		SDL_Rect getRect()
		{
			SDL_Rect rect;
			rect.x = this->x;
			rect.y = this->y;

			return rect;
		}

		int x, y;
	};
		
std::ostream& operator<<(std::ostream& os, const PM::Position& _position);

	namespace Global
	{

	const int SCREEN_BPP = 32;
	const int FPS = 25;

	const int AISlowDown = 20 * (1000 / PM::Global::FPS); // 700
	const int bombTimeToExplode = 75 * (1000 / PM::Global::FPS); // 3000
	const int bombTimeToVaporize = 13 * (1000 / PM::Global::FPS); // 520

	extern int FPSCount;

	extern int screenWidth;
	extern int screenHeight;
	
	extern SDL_Surface* textureSource;
	extern TTF_Font* font;
	extern SDL_Color fontColor;

	}

void initSDL();
void cleanUpSDL();
SDL_Surface* loadImage(const std::string& _filename);

void applySurface(int x, int y, SDL_Surface* source, SDL_Surface* destination);
void applySurface(SDL_Rect* sourceRect, SDL_Surface* source, SDL_Rect* targetRect, SDL_Surface* target);

SDL_Surface* createEmpySurface(int _weight, int _height);

	namespace Convert
	{

	std::string intToStr(int n);
	int strToInt(const std::string& str);

	}

template <typename type>
void swap(type& _a, type& _b)
{
	type pom;
	pom = _a;
	_a = _b;
	_b = pom;
}

}

#endif
