#include "map.h"

#include <iostream>
#include <fstream>
#include <cstdlib>
#include <ctime>

namespace PM
{

Map::Map(const std::string& _mapSourceFile):
	sourceFile(_mapSourceFile)
{}

Map::~Map()
{
	for(int y = 0; y < this->height; y++)
	{
		for(int x = 0; x < this->width; x++)
		{
			delete tiles[x][y];
		}
	}

	for(int i = 0; i < tiles.size(); i++)
	{
		tiles[i].clear();
	}
	
	tiles.clear();
}

int Map::build()
{
	int enemyCount = 0;
	this->wallsCount = 0;
	this->improvementsCount = 0;

	std::ifstream fin(this->sourceFile.c_str());

	std::string mapWidth, mapHeight;
	fin >> mapWidth;
	fin >> mapHeight;

	this->width = PM::Convert::strToInt(mapWidth) + 2;
	this->height = PM::Convert::strToInt(mapHeight) + 2;

	this->setScreenSize();

	// initialize tiles (2D std::vector) with NULL
	this->tiles.resize(this->width, std::vector< PM::Tile* >(this->height));

	std::string inputChar;
	TileType tileType;
	bool playerAdded = false;
	PM::Character* player = NULL;

	for(int y = 0; y < this->height; y++)
	{
		for(int x = 0; x < this->width; x++)
		{
			if(x == 0 || y == 0 || x == this->width - 1 || y == this->height - 1)
			{
				tileType = UWALL;
			}
			else
			{
				fin >> inputChar;
	
				if(inputChar == "0") tileType = PASSAGE;
				else if(inputChar == "1")
				{
					tileType = WALL;
					this->wallsCount++;
				}
				else if(inputChar == "2") tileType = UWALL;
				else if(inputChar == "p") tileType = PASSAGE;
				else if(inputChar == "b") tileType = PASSAGE;
				else throw PM::Exception("Bad type number in map declaration");
			}
			
			this->tiles[x][y] = new PM::Tile(*this, x, y, tileType);
			this->improvementsCount += this->tiles[x][y]->setImprovement();
			this->tiles[x][y]->signal_improvementFound.connect( sigc::mem_fun(*this, &Map::improvementFound) );
			this->tiles[x][y]->signal_improvementRemoved.connect( sigc::mem_fun(*this, &Map::improvementRemoved) );
			this->tiles[x][y]->signal_wallDestroyed.connect( sigc::mem_fun(*this, &Map::wallDestroyed) );

			if(inputChar == "p")
			{
				if(playerAdded == false)
				{
					// create player
					this->player = new PM::Pyroman(x, y);
					this->signal_addCharacter.emit(this->player);
					playerAdded = true;
				}
				// TODO Dodelat vyjimku !!!!
			}
			else if(inputChar == "b")
			{
				// create pyrobot
				this->signal_addCharacter.emit(new PM::Pyrobot(x, y, *this));
				enemyCount++;
			}

			inputChar.clear();
		}
	}
	
	srand(time(NULL));

	this->setViews();

	this->initialWallsCount = this->wallsCount;
	this->initialImprovementsCount = this->improvementsCount;

	return enemyCount;
}

void Map::setViews()
{
	PM::Tile* tile = NULL;
	PM::Position* position;

	for(int y = 1; y < this->height - 1; y++)
	{
		for(int x = 1; x < this->width - 1; x++)
		{
			position = new PM::Position(x, y);

			tile = this->getTile(*position);

			if(this->getTile(position->move(1))->getType() == PASSAGE)
			{
				tile->setView(0, this->getTile(position->move(1))->getView(0));
				tile->setViewDistance(0, this->getTile(position->move(1))->getViewDistance(0) + 1);
			}
			else
			{
				tile->setView(0, this->getTile(position->move(1)));
				tile->setViewDistance(0, 1);
			}

			if(this->getTile(position->move(3))->getType() == PASSAGE)
			{
				tile->setView(2, this->getTile(position->move(3))->getView(2));
				tile->setViewDistance(2, this->getTile(position->move(3))->getViewDistance(2) + 1);
			}
			else
			{
				tile->setView(2, this->getTile(position->move(3)));
				tile->setViewDistance(2, 1);
			}
		}
	}

	for(int y = this->height - 2; y > 0; y--)
	{
		for(int x = this->width - 2; x > 0; x--)
		{
			position = new PM::Position(x, y);

			tile = this->getTile(*position);

			if(this->getTile(position->move(2))->getType() == PASSAGE)
			{
				tile->setView(1, this->getTile(position->move(2))->getView(1));
				tile->setViewDistance(1, this->getTile(position->move(2))->getViewDistance(1) + 1);
			}
			else
			{
				tile->setView(1, this->getTile(position->move(2)));
				tile->setViewDistance(1, 1);
			}

			if(this->getTile(position->move(4))->getType() == PASSAGE)
			{
				tile->setView(3, this->getTile(position->move(4))->getView(3));
				tile->setViewDistance(3, this->getTile(position->move(4))->getViewDistance(3) + 1);
			}
			else
			{
				tile->setView(3, this->getTile(position->move(4)));
				tile->setViewDistance(3, 1);
			}
		}
	}
}

void Map::setScreenSize()
{
	PM::Global::screenWidth = this->width * 20;
	PM::Global::screenHeight = this->height * 20;
}

void Map::improvementFound(PM::Improvement* _improvement)
{
	this->signal_improvementFound.emit(_improvement);
}

void Map::improvementRemoved(PM::Improvement* _improvement)
{
	this->improvementsCount--;
	this->signal_improvementRemoved.emit(_improvement);
}

void Map::placeCharacter(PM::Character* _character)
{
	int x = _character->getPosition().x;
	int y = _character->getPosition().y;

	if(_character->getTile() == NULL)
	{
		tiles[x][y]->placeCharacter(_character);
	}
	else
	{
		if(tiles[x][y]->getType() != PASSAGE)
		{
			_character->setPosition(_character->getTile()->getPosition());
		}
		else
		{
			if(_character->getPosition() != _character->getTile()->getPosition())
			{
				_character->getTile()->removeCharacter(_character);
				tiles[x][y]->placeCharacter(_character);
			}
		}
	}
}

void Map::placeBomb(PM::Bomb* _bomb)
{
	PM::Position position = _bomb->getPosition();

	this->getTile(position)->placeBomb(_bomb);

	for(int i = 0; i < 4; i++)
	{
		position = _bomb->getPosition();
		for(int j = 0; j < _bomb->getRange(); j++)
		{
			position = position.move(i + 1);

			if(this->getTile(position)->getType() != UWALL)
			{
				// continue if the wall isn't clear (without bomb) wall
				if(this->getTile(position)->getType() != WALL || this->getTile(position)->isClear() == false)
				{
					this->getTile(position)->placeBomb(_bomb);
				}
				else
				{
					this->getTile(position)->placeBomb(_bomb);
					break;
				}
			}
			else
			{
				break;
			}
		}
	}
}

void Map::control()
{
	for(int y = 0; y < this->height; y++)
	{
		for(int x = 0; x < this->width; x++)
		{
			tiles[x][y]->control();
		}
	}
}

void Map::show(SDL_Surface* _surface)
{
	for(int y = 0; y < this->height; y++)
	{
		for(int x = 0; x < this->width; x++)
		{
			tiles[x][y]->show(_surface);
		}
	}
}

}
